<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Category');
		$this->template_data->set('current_uri', 'category');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('products', 'category', 'view');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("category?q=" . $this->input->get('q') ));
		}
	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$category = new $this->Products_category_model('i');

		if( $this->input->get('q') ) {
			$category->set_where('name LIKE "%' . $this->input->get('q') . '%"');
			$category->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$category->set_select("i.*");

		$category->set_order('i.name', 'ASC');
		$category->set_start($start);

		$this->template_data->set('categories', $category->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/category/index/'),
			'total_rows' => $category->count_all_results(),
			'per_page' => $category->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('category/category_list', $this->template_data->get_data());
	}


	public function add($output='') {

		$this->_isAuth('products', 'category', 'add');

		$this->template_data->set('output', $output);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Category Name', 'trim|required|is_unique[products_category.name]');
			if( $this->form_validation->run() ) {
				$category = new $this->Products_category_model;
				$category->setName($this->input->post('name'));
				$category->setTrolley($this->input->post('trolley'));
				$category->insert();
				$category_id = $category->get_inserted_id();
				redirect("category/edit/{$category_id}");
			}
			redirect("category");
		}
		$this->load->view('category/category_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('products', 'category', 'edit');

		$this->template_data->set('output', $output);

		$category = new $this->Products_category_model('i');
		$category->setId($id, true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('name', 'Category Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$category->setName($this->input->post('name'),false,true);
				$category->setTrolley($this->input->post('trolley'),false,true);
				$category->update();
			}
			$this->postNext();
		}

		$category->set_select("i.*");

		$this->template_data->set('item', $category->get());
		
		$this->load->view('category/category_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('products', 'category', 'delete');

		$category = new $this->Products_category_model;
		$category->setId($id, true);
		$category->delete();

		redirect( "category" );
	}

}
