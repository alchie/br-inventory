<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('User_accounts_model');
	}

	public function index() {

		$stats = new $this->User_accounts_model('ua');
		$stats->set_select('(SELECT count(*) FROM user_accounts) as users_count');
		$this->template_data->set('stats', $stats->get());
		
		$orders = new $this->Inventory_orders_model('p');
		$orders->set_select("p.*");
		$orders->set_order('p.date_order', 'DESC');
		$orders->set_select("(SELECT COUNT(*) FROM inventory_purchases WHERE order_id=p.id) as purchases");
		$orders->set_select("(SELECT SUM(s.price * s.quantity) FROM `inventory_stocks` s WHERE s.conn_id=p.id AND s.type='out') as budget");
		$this->template_data->set('orders', $orders->populate());

		$purchases = new $this->Inventory_purchases_model('p');
		$purchases->set_select("p.*");
		$purchases->set_order('p.date_purchase', 'DESC');
		$purchases->set_select("(SELECT SUM(s.price * s.quantity) FROM `inventory_stocks` s WHERE s.conn_id=p.id AND s.type='in') as cost");
		$this->template_data->set('purchases', $purchases->populate());

		$this->load->view('welcome/welcome', $this->template_data->get_data());
	}

}
