<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Inventory Items');
		$this->template_data->set('current_uri', 'items');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('products', 'items', 'view');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("items?q=" . $this->input->get('q') ));
		}
	}

	private function _getStart($text) {
		$text = trim($text);
		$alpha = strtolower(substr($text, 0, 1));
		$alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
		$alpha = (in_array($alpha, $alphabet)) ? $alpha : '1';
		return $alpha;
	}
	
	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$items = new $this->Products_items_model('i');
		$items->setActive(1,true);

		if( $this->input->get('q') ) {
			$items->set_where('item_name LIKE "%' . $this->input->get('q') . '%"');
			$items->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$items->set_select("i.*");

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

		$items->set_order('i.item_name', 'ASC');
		$items->set_start($start);

			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=i.item_id");
			$stocks->set_limit(1);
			$items->set_select("(".$stocks->get_compiled_select().") as stocks_items");

		$this->template_data->set('items', $items->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item("index_page") . '/items/index/'),
			'total_rows' => $items->count_all_results(),
			'per_page' => $items->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list', $this->template_data->get_data());
	}

	public function archived($start=0) {
		
		$this->template_data->set('page_id', "archived");

		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$items = new $this->Products_items_model('i');
		$items->setActive(0,true);

		if( $this->input->get('q') ) {
			$items->set_where('item_name LIKE "%' . $this->input->get('q') . '%"');
			$items->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$items->set_select("i.*");

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

		$items->set_order('i.item_name', 'ASC');
		$items->set_start($start);
		
			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=i.item_id");
			$stocks->set_limit(1);
			$items->set_select("(".$stocks->get_compiled_select().") as stocks_items");

		$this->template_data->set('items', $items->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item("index_page") . '/items/archived/'),
			'total_rows' => $items->count_all_results(),
			'per_page' => $items->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list', $this->template_data->get_data());
	}

	public function show_all() {
		
		$items = new $this->Products_items_model('i');
		$items->setActive(1,true);

		if( $this->input->get('q') ) {
			$items->set_where('item_name LIKE "%' . $this->input->get('q') . '%"');
			$items->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$items->set_select("i.*");

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

		$items->set_order('i.item_name', 'ASC');
		$items->set_limit(0);

		$this->template_data->set('items', $items->populate());

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list', $this->template_data->get_data());
	}

	public function print_all() {
		
		$items = new $this->Products_items_model('i');
		$items->setActive(1,true);

		if( $this->input->get('q') ) {
			$items->set_where('item_name LIKE "%' . $this->input->get('q') . '%"');
			$items->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$items->set_select("i.*");

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

		$items->set_limit(0);

		$order_by = ($this->input->get('order_by')) ? $this->input->get('order_by') : 'ASC';
		switch($this->input->get('sort')) {
			case 'item_id':
				$items->set_order('i.item_id', $order_by);
				break;
			case 'category':
				$items->set_order('pc.name', $order_by);
				$items->set_order('i.item_name', $order_by);	
				break;
			case 'item_name':
			default:
				$items->set_order('i.item_name', $order_by);
				break;
		}

		$this->template_data->set('items', $items->populate());

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list_print', $this->template_data->get_data());
	}


	public function category($category_id, $start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$items = new $this->Products_items_model('i');
		$items->setActive(1,true);
		$items->setCategoryId($category_id,true);

		if( $this->input->get('q') ) {
			$items->set_where('item_name LIKE "%' . $this->input->get('q') . '%"');
			$items->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$items->set_select("i.*");

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

		$items->set_order('i.item_name', 'ASC');
		$items->set_start($start);

			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=i.item_id");
			$stocks->set_limit(1);
			$items->set_select("(".$stocks->get_compiled_select().") as stocks_items");

		$this->template_data->set('items', $items->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item("index_page") . '/items/category/' . $category_id),
			'total_rows' => $items->count_all_results(),
			'per_page' => $items->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list', $this->template_data->get_data());
	}

	public function store($store_id, $start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$items = new $this->Products_items_model('i');
		$items->setStoreId($store_id,true);
		$items->setActive(1,true);

		if( $this->input->get('q') ) {
			$items->set_where('item_name LIKE "%' . $this->input->get('q') . '%"');
			$items->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$items->set_select("i.*");

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

		$items->set_order('i.item_name', 'ASC');
		$items->set_start($start);

			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=i.item_id");
			$stocks->set_limit(1);
			$items->set_select("(".$stocks->get_compiled_select().") as stocks_items");

		$this->template_data->set('items', $items->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item("index_page") . '/items/store/' . $store_id),
			'total_rows' => $items->count_all_results(),
			'per_page' => $items->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list', $this->template_data->get_data());
	}

	public function starts($alpha, $start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$items = new $this->Products_items_model('i');
		$items->setActive(1,true);
		$items->set_select("i.*");

		$items->set_order('item_name', 'ASC');
		$items->set_start($start);
		$items->setStart($this->_getStart($alpha),true);

		$items->set_join('products_category pc', 'pc.id=i.category_id');
		$items->set_select("pc.name as category_name");

		$items->set_join('products_store ps', 'ps.id=i.store_id');
		$items->set_select("ps.name as store_name");

			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=i.item_id");
			$stocks->set_limit(1);
			$items->set_select("(".$stocks->get_compiled_select().") as stocks_items");
			
		$this->template_data->set('items', $items->populate());

		$this->template_data->set('alpha_start', $alpha);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item("index_page") . '/items/starts/' . $alpha),
			'total_rows' => $items->count_all_results(),
			'per_page' => $items->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('all_items', $items->count_all_results());

		$this->load->view('items/items_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('products', 'items', 'add');

		$this->template_data->set('output', $output);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|is_unique[products_items.item_name]');
			$item_id = 0;
			if( $this->form_validation->run() ) {
				$item = new $this->Products_items_model;
				$item->setItemName($this->input->post('item_name'));
				$item->setNetWeight($this->input->post('net_weight'));
				$item->setCategoryId($this->input->post('category_id'));
				$item->setContent($this->input->post('content'));
				$item->setStoreId($this->input->post('store_id'));
				$item->setMinimum($this->input->post('minimum'));
				$item->setShelf($this->input->post('shelf'));
				$item->setStart($this->_getStart($this->input->post('item_name')));
				$item->insert();
				$item_id = $item->get_inserted_id();
				//redirect("items/edit/{$item_id}");
			}
			$this->postNext("new_product={$item_id}");
			redirect( site_url("items") . "?new_product={$item_id}" );
		}

		$category = new $this->Products_category_model('c');
		$category->set_select("c.*");
		$category->set_order('c.name', 'ASC');
		$category->set_limit(0);
		$this->template_data->set('categories', $category->populate());

		$store = new $this->Products_store_model('s');
		$store->set_select("s.*");
		$store->set_order('s.name', 'ASC');
		$store->set_limit(0);
		$this->template_data->set('stores', $store->populate());

		$this->load->view('items/items_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('products', 'items', 'edit');

		$this->template_data->set('output', $output);

		$item = new $this->Products_items_model('i');
		$item->setItemId($id, true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$item->setItemName($this->input->post('item_name'),false,true);
				$item->setNetWeight($this->input->post('net_weight'),false,true);
				$item->setContent($this->input->post('content'),false,true);
				$item->setCategoryId($this->input->post('category_id'),false,true);
				$item->setStoreId($this->input->post('store_id'),false,true);
				$item->setMinimum($this->input->post('minimum'),false,true);
				$item->setShelf($this->input->post('shelf'),false,true);
				$item->setStart($this->_getStart($this->input->post('item_name')),false,true);
				$item->update();
			}
			$this->postNext("update_product={$id}");
		}

		$item->set_select("i.*");

			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=i.item_id");
			$stocks->set_limit(1);
			$item->set_select("(".$stocks->get_compiled_select().") as stocks_items");

		$this->template_data->set('item', $item->get());

		$category = new $this->Products_category_model('c');
		$category->set_select("c.*");
		$category->set_order('c.name', 'ASC');
		$category->set_limit(0);
		$this->template_data->set('categories', $category->populate());

		$store = new $this->Products_store_model('s');
		$store->set_select("s.*");
		$store->set_order('s.name', 'ASC');
		$store->set_limit(0);
		$this->template_data->set('stores', $store->populate());

		$this->load->view('items/items_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('products', 'items', 'delete');

		$item = new $this->Products_items_model();
		$item->setItemId($id, true);

			$stocks = new $this->Inventory_stocks_model('is');
			$stocks->set_select("COUNT(*)");
			$stocks->set_where("is.item_id=products_items.item_id");
			$stocks->set_limit(1);
		$item->set_select("(".$stocks->get_compiled_select().") as stocks_items");
			
		$item_data = $item->get();
		if( $item_data->stocks_items == 0 ) {
				$item->delete();
		}

		redirect( $this->input->get('next') );
	}

	public function deactivate($id) {

		$this->_isAuth('products', 'items', 'edit');

		$item = new $this->Products_items_model();
		$item->setItemId($id, true);
		$item->setActive('0',false,true);
		$item->update();

		redirect( site_url( $this->input->get('next') ) );
	}

	public function activate($id) {
		
		$this->_isAuth('products', 'items', 'edit');

		$item = new $this->Products_items_model();
		$item->setItemId($id, true);
		$item->setActive('1',false,true);
		$item->update();

		redirect( $this->input->get('next') );
	}

}
