<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Orders');
		$this->template_data->set('current_uri', 'orders');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('inventory', 'orders', 'view');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("orders?q=" . $this->input->get('q') ));
		}
	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$orders = new $this->Inventory_orders_model('p');
		$orders->set_select("p.*");
		$orders->set_order('p.date_order', 'DESC');
		$orders->set_start($start);
		$orders->set_select("(SELECT COUNT(*) FROM inventory_purchases WHERE order_id=p.id) as purchases");
		$orders->set_select("(SELECT SUM(s.price * s.quantity) FROM `inventory_stocks` s WHERE s.conn_id=p.id AND s.type='out') as budget");

		$this->template_data->set('orders', $orders->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/orders/index/'),
			'total_rows' => $orders->count_all_results(),
			'per_page' => $orders->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('orders/orders_list', $this->template_data->get_data());
	}


	public function add($output='') {

		$this->_isAuth('inventory', 'orders', 'add');

		$this->template_data->set('output', $output);

		if( $this->input->post() ) {

			$this->form_validation->set_rules('date_order', 'Order Date', 'trim|required');
			if( $this->form_validation->run() ) {
				$orders = new $this->Inventory_orders_model;
				$orders->setDateOrder(date("Y-m-d", strtotime($this->input->post('date_order'))));
				$orders->setRequester($this->input->post('requester'));
				$orders->insert();
				$orders_id = $orders->get_inserted_id();
				redirect("orders/items/{$orders_id}");
			}
			redirect("orders");
		}
		$this->load->view('orders/orders_add', $this->template_data->get_data());	
	}

	public function edit($id,$output='') {

		$this->_isAuth('inventory', 'orders', 'edit');

		$this->template_data->set('output', $output);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('date_order', 'Order Date', 'trim|required');
			if( $this->form_validation->run() ) {
				$orders->setDateOrder(date("Y-m-d", strtotime($this->input->post('date_order'))),false,true);
				$orders->setRequester($this->input->post('requester'),false,true);
				$orders->update();
			}
			$this->postNext();
		}

		$orders->set_select("p.*");
		$orders->set_select("(SELECT COUNT(*) FROM inventory_purchases WHERE order_id=p.id) as purchases");

		$this->template_data->set('order', $orders->get());
		
		$this->load->view('orders/orders_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('inventory', 'orders', 'delete');

		$orders = new $this->Inventory_orders_model;
		$orders->setId($id, true);
		$orders->delete();

		redirect( "orders" );
	}

	public function items($id, $start=0) {

		$this->_isAuth('inventory', 'orders', 'edit');

		$this->template_data->set('order_id', $id);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);
		$orders->set_select("p.*");
		$orders->set_select("(SELECT COUNT(*) FROM inventory_purchases WHERE order_id=p.id) as purchases");
		$this->template_data->set('order', $orders->get());

		$stocks = new $this->Inventory_stocks_model('i');
		$stocks->setConnId($id, true);
		$stocks->setType('out', true);
		$stocks->set_select("i.*");
		$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
		$stocks->set_select("pi.item_name as item_name");
		$stocks->set_select("pi.net_weight as net_weight");
		$stocks->set_order('i.id', 'ASC');
		$stocks->set_limit(0);
		if($this->input->get('remaining')==1) {
			$stocks->set_select("((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}')))) as purchased_items");
			//$stocks->set_select("((SELECT(SUM(i.quantity))) - (IF((((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}'))))  IS NULL),0, ((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}')))) )) ) as remaining_quantity");
		}
		$this->template_data->set('stocks', $stocks->populate());

		$last_id = new $this->Inventory_stocks_model('is');
		$last_id->set_order('receipt_id', 'DESC');
		$last_id->set_order('item_date', 'DESC');
		$last_id_data = $last_id->get();
		$this->template_data->set('new_id', ($last_id_data->receipt_id+1));

		$this->load->view('orders/orders_items', $this->template_data->get_data());
	}

	public function items_print($id, $start=0) {

		$this->_isAuth('inventory', 'orders', 'edit');

		$this->template_data->set('order_id', $id);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);
		$orders->set_select("p.*");
		$orders->set_select("(SELECT COUNT(*) FROM inventory_purchases WHERE order_id=p.id) as purchases");
		$this->template_data->set('order', $orders->get());

		$stocks = new $this->Inventory_stocks_model('i');
		$stocks->setConnId($id, true);
		$stocks->setType('out', true);
		$stocks->set_select("i.*");
		$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
		$stocks->set_select("pi.item_name as item_name");
		$stocks->set_select("pi.net_weight as net_weight");
		$stocks->set_select("pi.content as content");
		$stocks->set_join('products_category pc', 'pc.id=pi.category_id');
		$stocks->set_select("pc.name as category_name");
		$stocks->set_join('products_store ps', 'ps.id=pi.store_id');
		$stocks->set_select("ps.name as store_name");
		$stocks->set_group_by('i.item_id');
		$stocks->set_select("(SUM(i.quantity)) as total_quantity");
		$stocks->set_select("(SUM( i.quantity * i.price ) ) as total_amount");
		$stocks->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=i.item_id AND si.type LIKE 'in' ORDER BY si.price DESC LIMIT 1) as last_price");
 
		if($this->input->get('trolley')) {
			$stocks->set_where("pc.trolley", $this->input->get('trolley'));
		}

		if($this->input->get('remaining')==1) {
			$stocks->set_select("((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}')))) as purchased_items");
			$stocks->set_select("((SELECT(SUM(i.quantity))) - (IF((((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}'))))  IS NULL),0, ((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}')))) )) ) as remaining_quantity");
			//$stocks->set_where("((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}'))) IS NULL)");
			//$stocks->set_where("( ((SELECT(SUM(i.quantity))) - (IF((((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}'))))  IS NULL),0, ((SELECT SUM(is2.quantity) FROM  `inventory_stocks` is2 JOIN inventory_purchases ip2 ON ip2.id = is2.conn_id WHERE ((is2.item_id=i.item_id) AND (is2.type LIKE 'in') AND (ip2.order_id='{$id}')))) )) ) > 0)");

		}
		
		$stocks->set_order('pc.trolley', 'ASC');

		if($this->input->get('check')==1) {
			$stocks->set_order('i.item_id', 'ASC');
			$stocks->set_group_by('i.price');
		} else {

			$order_by = ($this->input->get('order_by')) ? $this->input->get('order_by') : 'ASC';

			if( $this->input->get('sort') == 'store' ) {
				$stocks->set_order('ps.id', $order_by);
			} else {
				$stocks->set_order('ps.id', 'DESC');
			}
			
			switch($this->input->get('sort')) {
				case 'item_id':
					$stocks->set_order('i.item_id', $order_by);
					break;
				case 'item_name':
					$stocks->set_order('pi.item_name', $order_by);
					break;
				case 'net_weight':
					$stocks->set_order('pi.net_weight', $order_by);
					break;
				case 'quantity':
					$stocks->set_order('(SUM(i.quantity))', $order_by);
					break;
				case 'price':
					$stocks->set_order('i.price', $order_by);
					break;
				case 'total':
					$stocks->set_order('(SUM( i.quantity * i.price ) )', $order_by);	
					break;
				case 'category':
				default:
					$stocks->set_order('pc.name', $order_by);
					$stocks->set_order('pi.item_name', $order_by);
					break;
			}

		}
		$stocks->set_limit(0);

		$this->template_data->set('stocks', $stocks->populate());


		$stocks->set_group_by('pc.trolley',true);
		$stocks->set_order('pc.trolley', 'ASC');
		$stocks->set_select("pc.trolley", NULL, true);
		$this->template_data->set('trolley', $stocks->populate());


		$this->load->view('orders/orders_items_print', $this->template_data->get_data());
	}

	public function add_item($id, $output='') {
		$this->template_data->set('order_id', $id);
		$this->template_data->set('output', $output);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);
		$orders->set_select("p.*");
		$orders->set_select("(SELECT SUM(s.quantity * s.price) FROM inventory_stocks s WHERE s.conn_id=p.id AND s.type LIKE 'out') as total");
		$this->template_data->set('order', $orders->get());

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('receipt_id', 'Receipt ID', 'trim|required');
			$this->form_validation->set_rules('item_id', 'Item', 'trim|required');
			$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
			if( $this->form_validation->run() ) {
				$item = new $this->Inventory_stocks_model('is');
				$item->setConnId($id);
				$item->setReceiptId($this->input->post('receipt_id'));
				$item->setItemId($this->input->post('item_id'));
				$item->setItemDate(date('Y-m-d', strtotime($this->input->post('item_date'))));
				$item->setQuantity($this->input->post('quantity'));

				$hightest_price = new $this->Inventory_stocks_model('is');
				$hightest_price->setItemId($this->input->post('item_id'),true);
				$hightest_price->setType("in",true);
				$hightest_price->set_join("products_items pi", "pi.item_id=is.item_id");
				$hightest_price->set_order('is.price', 'DESC');
				$hightest_price->set_select("is.price");
				$hightest_price->set_select("pi.content");
				$price = $hightest_price->get();

				$item->setPrice(($price->price / $price->content));
				$item->setType('out');
				$item->insert();
				$new_item = $item->get_inserted_id();
			}
			$this->postNext();
			redirect( site_url("orders/add_item/{$id}") . "?new_item={$new_item}");
		}

		if( $this->input->get('new_item') ) {
			$new_data = new $this->Inventory_stocks_model('is');
			$new_data->setId($this->input->get('new_item'),true);
			$new_data->set_join('products_items pi', 'pi.item_id=is.item_id');
			$this->template_data->set('new_item', $new_data->get());
		}

		if( $this->input->get('new_product') || $this->input->get('update_product') ) {
			$product = new $this->Products_items_model;
			$product_id = ($this->input->get('new_product')) ? $this->input->get('new_product') : $this->input->get('update_product');
			$product->setItemId($product_id,true);
			$this->template_data->set('new_product', $product->get());
		}

		$last_id = new $this->Inventory_stocks_model('is');
		//$last_id->set_order('receipt_id', 'DESC');
		//$last_id->set_order('item_date', 'DESC');
		$last_id->set_order('id', 'DESC');
		$last_id->setType('out', true);
		$this->template_data->set('last_id', $last_id->get());

		$products = new $this->Products_items_model('pi');
		$products->set_limit(0);
		$products->setActive(1,true);
		$products->set_select("*");
		$products->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=pi.item_id AND si.type LIKE 'in' ORDER BY si.price DESC LIMIT 1) as last_price");
		$products->set_order('pi.item_name', 'ASC');
		$this->template_data->set('items', $products->populate());

		$last_added = new $this->Inventory_stocks_model('i');
		$last_added->setConnId($id, true);
		$last_added->setType('out', true);
		$last_added->set_select("i.*");
		$last_added->set_join("products_items pi", 'pi.item_id=i.item_id');
		$last_added->set_select("pi.item_name as item_name");
		$last_added->set_select("pi.net_weight as net_weight");
		$last_added->set_order('i.id', 'DESC');
		$last_added->set_limit(10);
		$this->template_data->set('last_added', $last_added->populate());

		$this->load->view('orders/orders_add_item', $this->template_data->get_data());
	}

	public function add_item2($id, $receipt_id) {

		$this->template_data->set('order_id', $id);
		$this->template_data->set('receipt_id', $receipt_id);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);
		$orders->set_select("p.*");
		$orders->set_select("(SELECT SUM(s.quantity * s.price) FROM inventory_stocks s WHERE s.conn_id=p.id AND s.type LIKE 'out') as total");
		$this->template_data->set('order', $orders->get());

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('receipt_id', 'Receipt ID', 'trim|required');
			$this->form_validation->set_rules('item_id', 'Item', 'trim|required');
			$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
			if( $this->form_validation->run() ) {
				$item = new $this->Inventory_stocks_model('is');
				$item->setConnId($id);
				$item->setReceiptId($this->input->post('receipt_id'));
				$item->setItemId($this->input->post('item_id'));
				$item->setItemDate(date('Y-m-d', strtotime($this->input->post('item_date'))));
				$item->setQuantity($this->input->post('quantity'));

				$hightest_price = new $this->Inventory_stocks_model('is');
				$hightest_price->setItemId($this->input->post('item_id'),true);
				$hightest_price->setType("in",true);
				$hightest_price->set_join("products_items pi", "pi.item_id=is.item_id");
				$hightest_price->set_order('is.price', 'DESC');
				$hightest_price->set_select("is.price");
				$hightest_price->set_select("pi.content");
				$price = $hightest_price->get();

				$item->setPrice(($price->price / $price->content));
				$item->setType('out');
				$item->insert();
				$new_item = $item->get_inserted_id();
			}
			$this->postNext();
			redirect( site_url("orders/add_item/{$id}") . "?new_item={$new_item}");
		}

		if( $this->input->get('new_item') ) {
			$new_data = new $this->Inventory_stocks_model('is');
			$new_data->setId($this->input->get('new_item'),true);
			$new_data->set_join('products_items pi', 'pi.item_id=is.item_id');
			$this->template_data->set('new_item', $new_data->get());
		}

		if( $this->input->get('new_product') || $this->input->get('update_product') ) {
			$product = new $this->Products_items_model;
			$product_id = ($this->input->get('new_product')) ? $this->input->get('new_product') : $this->input->get('update_product');
			$product->setItemId($product_id,true);
			$this->template_data->set('new_product', $product->get());
		}

		$last_id = new $this->Inventory_stocks_model('is');
		$last_id->set_order('receipt_id', 'DESC');
		$last_id->set_order('item_date', 'DESC');
		$this->template_data->set('last_id', $last_id->get());

		$products = new $this->Products_items_model('pi');
		$products->set_limit(0);
		$products->setActive(1,true);
		$products->set_select("*");
		$products->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=pi.item_id AND si.type LIKE 'in' ORDER BY si.price DESC LIMIT 1) as last_price");
		$products->set_order('pi.item_name', 'ASC');
		$this->template_data->set('items', $products->populate());

		$last_added = new $this->Inventory_stocks_model('i');
		$last_added->setConnId($id, true);
		$last_added->setType('out', true);
		$last_added->set_select("i.*");
		$last_added->set_join("products_items pi", 'pi.item_id=i.item_id');
		$last_added->set_select("pi.item_name as item_name");
		$last_added->set_select("pi.net_weight as net_weight");
		$last_added->set_order('i.id', 'DESC');
		$last_added->set_limit(10);
		$this->template_data->set('last_added', $last_added->populate());

		$this->load->view('orders/orders_add_item2', $this->template_data->get_data());
	}

	public function edit_item($id, $output='') {
		$this->template_data->set('purchase_id', $id);
		$this->template_data->set('output', $output);

		$item = new $this->Inventory_stocks_model('is');
		$item->setId($id,true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('receipt_id', 'Receipt ID', 'trim|required');
			$this->form_validation->set_rules('item_id', 'Item', 'trim|required');
			$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
			if( $this->form_validation->run() ) {
				$item->setItemDate(date('Y-m-d', strtotime($this->input->post('item_date'))),false,true);
				$item->setReceiptId($this->input->post('receipt_id'),false,true);
				$item->setItemId($this->input->post('item_id'),false,true);
				$item->setQuantity($this->input->post('quantity'),false,true);
				$item->setPrice(str_replace(",", "", $this->input->post('price')),false,true);
				$item->update();
			}
			$this->postNext("new_item={$id}");
		}

		$this->template_data->set('current_item', $item->get());

		$products = new $this->Products_items_model('pi');
		$products->set_limit(0);
		$products->setActive(1,true);
		$products->set_select("*");
		$products->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=pi.item_id  AND si.type LIKE 'in' ORDER BY si.price DESC LIMIT 1) as last_price");
		$products->set_order('pi.item_name', 'ASC');
		$this->template_data->set('items', $products->populate());

		$this->load->view('orders/orders_edit_item', $this->template_data->get_data());
	}

	public function delete_item($id) {

		$this->_isAuth('inventory', 'orders', 'delete');

		$item = new $this->Inventory_stocks_model;
		$item->setId($id, true);
		$item->delete();

		redirect($this->input->get('next'));
	}

	public function choose_dates($output='') {

		if( $this->input->post() ) {
			$date_start = ($this->input->post("date_start")) ? date('Y-m-d', strtotime($this->input->post("date_start"))) : date('Y-m-d');
			$date_end = ($this->input->post("date_end")) ? date('Y-m-d', strtotime($this->input->post("date_end"))) : date('Y-m-d');
			$by_category = ($this->input->post("by_category")) ? 1 : 0;

			redirect("orders/report_dates/{$date_start}/{$date_end}/{$by_category}");
		}

		$this->template_data->set('output', $output);
		$this->load->view('orders/orders_choose_dates', $this->template_data->get_data());
	}

	public function report_dates($start_date, $end_date, $by_cat=0, $start=0) {

		$this->template_data->set('start_date', $start_date);
		$this->template_data->set('end_date', $end_date);
		$this->template_data->set('by_cat', $by_cat);

		$this->_isAuth('inventory', 'orders', 'edit');

		$dates = new $this->Inventory_stocks_model('i');
		$dates->setType('out', true);
		$dates->set_select("(DATE_FORMAT(i.item_date, \"%Y-%m-%d\")) as item_date");
		$dates->set_group_by("(DATE_FORMAT(i.item_date, \"%Y-%m-%d\"))");
		$dates->set_order("i.item_date", 'ASC');
		$dates->set_where("i.item_date IS NOT NULL");
		$dates->set_where("i.item_date >= '".date("Y-m-d", strtotime($start_date))."'");
		$dates->set_where("i.item_date <= '" . date("Y-m-d", strtotime($end_date))."'");

		$dates->set_limit(($by_cat)?1:5);
		$dates->set_start($start);
		$dates->set_where("i.quantity IS NOT NULL");
		$dates_data = $dates->populate();
		$this->template_data->set('dates', $dates_data);
		if( $dates_data ) {
			if( $by_cat ) {
				$category = new $this->Inventory_stocks_model('i');
				$category->setType('out', true);
				$category->set_group_by("pc.id");
				$category->set_order("pc.name", 'ASC');
				$category->set_where("i.item_date IS NOT NULL");
				if( $by_cat ) {
					$category->set_where("i.item_date = '".date("Y-m-d", strtotime($dates_data[0]->item_date))."'");
				} else {
					$category->set_where("i.item_date >= '".date("Y-m-d", strtotime($start_date))."'");
					$category->set_where("i.item_date <= '" . date("Y-m-d", strtotime($end_date))."'");
				}
				$category->set_limit(0);
				$category->set_join("products_items pi", 'pi.item_id=i.item_id');
				$category->set_join('products_category pc', 'pc.id=pi.category_id');
				$category->set_select("pc.id as category_id");
				$category->set_select("pc.name as category_name");
				if($this->input->get('filter_category')) {
					$category->set_where("pc.id", $this->input->get('filter_category'));
				}
				$category->set_where("i.quantity IS NOT NULL");
				$this->template_data->set('categories', $category->populate());

			} 

				$this->template_data->set('pagination', bootstrap_pagination(array(
					'uri_segment' => 6,
					'base_url' => base_url($this->config->item("index_page") . "/orders/report_dates/{$start_date}/{$end_date}/{$by_cat}"),
					'total_rows' => $dates->count_all_results(),
					'per_page' => $dates->get_limit(),
					'ajax'=>true,
				)));

			$items = new $this->Inventory_stocks_model('i');
			$items->setType('out', true);
			$items->set_select("i.*");
			$items->set_join("products_items pi", 'pi.item_id=i.item_id');
			$items->set_select("pi.item_name as item_name");
			$items->set_select("pi.net_weight as net_weight");
			$items->set_join('products_category pc', 'pc.id=pi.category_id');
			$items->set_select("pc.name as category_name");
			$items->set_select("pc.id as category_id");
			$items->set_join('products_store ps', 'ps.id=pi.category_id');
			$items->set_select("ps.name as store_name");
			$items->set_limit(0);
			$items->set_group_by('i.item_id');

			$order_by = ($this->input->get('order_by')) ? $this->input->get('order_by') : 'ASC';
			
				switch($this->input->get('sort')) {
					case 'item_id':
						$items->set_order('i.item_id', $order_by);
						break;
					case 'item_name':
					default:
						$items->set_order('pi.item_name', $order_by);
						break;
				}

			$items->set_where("i.quantity IS NOT NULL");
			//$items->set_where("i.item_date IS NOT NULL");
			if( $by_cat ) {
				$items->set_where("i.item_date = '".date("Y-m-d", strtotime($dates_data[0]->item_date))."'");
			} else {
				$items->set_where("i.item_date >= '".date("Y-m-d", strtotime($start_date))."'");
				$items->set_where("i.item_date <= '" . date("Y-m-d", strtotime($end_date))."'");
			}
			if($this->input->get('filter_category')) {
				$items->set_where("pc.id", $this->input->get('filter_category'));
			}
			$this->template_data->set('items', $items->populate());

			$stocks = new $this->Inventory_stocks_model('i');
			$stocks->setType('out', true);
			$stocks->set_select("i.*");
			$stocks->set_select("( i.quantity * i.price ) as total_amount");	

			$stocks->set_where("i.quantity IS NOT NULL");
			if( $by_cat ) {
				$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
				$stocks->set_join('products_category pc', 'pc.id=pi.category_id');
				$stocks->set_select("pc.id as category_id");
				$stocks->set_where("i.item_date = '".date("Y-m-d", strtotime($dates_data[0]->item_date))."'");
			} else {
				$stocks->set_where("i.item_date >= '".date("Y-m-d", strtotime($start_date))."'");
				$stocks->set_where("i.item_date <= '" . date("Y-m-d", strtotime($end_date))."'");
			}
			if($this->input->get('filter_category')) {
				$stocks->set_where("pc.id", $this->input->get('filter_category'));
			}
			$stocks->set_limit(0);
			$this->template_data->set('stocks', $stocks->populate());

			if( $by_cat ) {
				$this->load->view('orders/orders_report_dates_by_category', $this->template_data->get_data());
			} else {
				$this->load->view('orders/orders_report_dates', $this->template_data->get_data());
			}
		} else {
			$this->load->view('orders/orders_report_dates_no_items', $this->template_data->get_data());
		}

	}

	public function analytics($id, $start=0) {

		$this->_isAuth('inventory', 'orders', 'edit');

		$this->template_data->set('order_id', $id);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);
		$orders->set_select("p.*");
		$orders->set_select("(SELECT COUNT(*) FROM inventory_purchases WHERE order_id=p.id) as purchases");
		$this->template_data->set('order', $orders->get());

		$dates = new $this->Inventory_stocks_model('i');
		$dates->setConnId($id, true);
		$dates->setType('out', true);
		$dates->set_select("(DATE_FORMAT(i.item_date, \"%Y-%m-%d\")) as item_date");
		$dates->set_group_by("(DATE_FORMAT(i.item_date, \"%Y-%m-%d\"))");
		$dates->set_order("i.item_date", 'ASC');
		$dates->set_where("i.item_date IS NOT NULL");
		$dates->set_limit(($this->input->get('limit'))?$this->input->get('limit'):5);
		$dates->set_start($start);
		$this->template_data->set('dates', $dates->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item("index_page") . '/orders/analytics/' . $id),
			'total_rows' => $dates->count_all_results(),
			'per_page' => $dates->get_limit(),
			'ajax'=>true,
		)));

		$items = new $this->Inventory_stocks_model('i');
		$items->setConnId($id, true);
		$items->setType('out', true);
		$items->set_select("i.*");
		$items->set_join("products_items pi", 'pi.item_id=i.item_id');
		$items->set_select("pi.item_name as item_name");
		$items->set_select("pi.net_weight as net_weight");
		$items->set_join('products_category pc', 'pc.id=pi.category_id');
		$items->set_select("pc.name as category_name");
		$items->set_join('products_store ps', 'ps.id=pi.category_id');
		$items->set_select("ps.name as store_name");
		$items->set_limit(0);
		$items->set_group_by('i.item_id');

		$order_by = ($this->input->get('order_by')) ? $this->input->get('order_by') : 'ASC';
		
			switch($this->input->get('sort')) {
				case 'item_id':
					$items->set_order('i.item_id', $order_by);
					break;
				case 'item_name':
				default:
					$items->set_order('pi.item_name', $order_by);
					break;
			}

		$this->template_data->set('items', $items->populate());

		$stocks = new $this->Inventory_stocks_model('i');
		$stocks->setConnId($id, true);
		$stocks->setType('out', true);
		$stocks->set_select("i.*");
		$stocks->set_select("( i.quantity * i.price ) as total_amount");
		$stocks->set_limit(0);
		$this->template_data->set('stocks', $stocks->populate());

		$this->load->view('orders/orders_items_analytics', $this->template_data->get_data());
	}

}
