<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Purchases');
		$this->template_data->set('current_uri', 'purchases');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('inventory', 'purchases', 'view');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("purchases?q=" . $this->input->get('q') ));
		}
	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$purchases = new $this->Inventory_purchases_model('p');
		$purchases->set_select("p.*");
		$purchases->set_order('p.date_purchase', 'DESC');
		$purchases->set_start($start);
		$purchases->set_select("(SELECT SUM(s.price * s.quantity) FROM `inventory_stocks` s WHERE s.conn_id=p.id AND s.type='in') as cost");

		$this->template_data->set('purchases', $purchases->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/purchases/index/'),
			'total_rows' => $purchases->count_all_results(),
			'per_page' => $purchases->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('purchases/purchases_list', $this->template_data->get_data());
	}


	public function add($output='') {

		$this->_isAuth('inventory', 'purchases', 'add');

		$this->template_data->set('output', $output);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('date_purchase', 'Purchase Date', 'trim|required');
			if( $this->form_validation->run() ) {
				$purchases = new $this->Inventory_purchases_model;
				$purchases->setDatePurchase(date("Y-m-d", strtotime($this->input->post('date_purchase'))));
				$purchases->setPurchaser($this->input->post('purchaser'));
				if( $this->input->get('order_id') ) {
					$purchases->setOrderId($this->input->get('order_id'));
				}
				$purchases->insert();
				$purchases_id = $purchases->get_inserted_id();
				$this->postNext();
				redirect("purchases/items/{$purchases_id}");
			}
			$this->postNext();
			redirect("purchases");
		}

		$this->load->view('purchases/purchases_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('inventory', 'purchases', 'edit');

		$this->template_data->set('output', $output);

		$purchases = new $this->Inventory_purchases_model('p');
		$purchases->setId($id, true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('date_purchase', 'Purchase Date', 'trim|required');
			if( $this->form_validation->run() ) {
				$purchases->setDatePurchase(date("Y-m-d", strtotime($this->input->post('date_purchase'))),false,true);
				$purchases->setPurchaser($this->input->post('purchaser'),false,true);
				$purchases->setOrderId($this->input->post('order_id'),false,true);
				$purchases->update();
			}
			$this->postNext();
		}

		$purchases->set_select("p.*");
		$this->template_data->set('purchase', $purchases->get());
		
		$this->load->view('purchases/purchases_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('inventory', 'purchases', 'delete');

		$purchases = new $this->Inventory_purchases_model;
		$purchases->setId($id, true);
		$purchases->delete();

		redirect( "purchases" );
	}

	public function items($id, $start=0) {

		$this->_isAuth('inventory', 'purchases', 'edit');

		$this->template_data->set('purchase_id', $id);

		$purchases = new $this->Inventory_purchases_model('p');
		$purchases->setId($id, true);
		$this->template_data->set('purchase', $purchases->get());

		$stocks = new $this->Inventory_stocks_model('i');
		$stocks->setConnId($id, true);
		$stocks->setType('in', true);
		$stocks->set_select("i.*");
		$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
		$stocks->set_select("pi.item_name as item_name");
		$stocks->set_select("pi.net_weight as net_weight");
		$stocks->set_order('i.id', 'ASC');
		$stocks->set_limit(0);
		$this->template_data->set('stocks', $stocks->populate());

		$this->load->view('purchases/purchases_items', $this->template_data->get_data());
	}

	public function order($id, $start=0) {

		$this->_isAuth('inventory', 'purchases', 'edit');

		$this->template_data->set('order_id', $id);

		$orders = new $this->Inventory_orders_model('p');
		$orders->setId($id, true);
		$this->template_data->set('order', $orders->get());

		$stocks = new $this->Inventory_stocks_model('i');
		$stocks->setType('in', true);
		$stocks->set_select("i.*");
		$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
		$stocks->set_select("pi.item_name as item_name");
		$stocks->set_select("pi.net_weight as net_weight");
		$stocks->set_join("inventory_purchases ip", 'ip.id=i.conn_id');
		$stocks->set_select("ip.date_purchase");
		$stocks->set_select("ip.id as purchase_id");
		$stocks->set_order('i.id', 'ASC');
		$stocks->set_where('ip.order_id', $id);
		$stocks->set_limit(0);
		$this->template_data->set('stocks', $stocks->populate());

		$this->load->view('purchases/purchases_items_order', $this->template_data->get_data());
	}

	public function items_print($id, $start=0) {

		$this->_isAuth('inventory', 'purchases', 'edit');

		$this->template_data->set('purchase_id', $id);

		$purchases = new $this->Inventory_purchases_model('p');
		$purchases->setId($id, true);
		$this->template_data->set('purchase', $purchases->get());

		$stocks = new $this->Inventory_stocks_model('i');
		$stocks->setConnId($id, true);
		$stocks->setType('in', true);
		$stocks->set_select("i.*");
		$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
		$stocks->set_select("pi.item_name as item_name");
		$stocks->set_select("pi.net_weight as net_weight");
		$stocks->set_join('products_category pc', 'pc.id=pi.category_id');
		$stocks->set_select("pc.name as category_name");
		$stocks->set_order('i.id', 'ASC');
		$stocks->set_limit(0);
		$this->template_data->set('stocks', $stocks->populate());

		$this->load->view('purchases/purchases_items_print', $this->template_data->get_data());
	}

	public function add_item($id, $output='') {
		$this->template_data->set('purchase_id', $id);
		$this->template_data->set('output', $output);

		$purchases = new $this->Inventory_purchases_model('p');
		$purchases->setId($id, true);
		$purchases->set_select("p.*");
		$purchases->set_select("(SELECT SUM(s.quantity * s.price) FROM inventory_stocks s WHERE s.conn_id=p.id AND s.type LIKE 'in') as total");
		$purchase_data = $purchases->get();
		$this->template_data->set('purchase', $purchase_data);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('item_id', 'Item', 'trim|required');
			$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
			$this->form_validation->set_rules('price', 'Price per unit', 'trim|required');
			if( $this->form_validation->run() ) {
				$item = new $this->Inventory_stocks_model('is');
				$item->setConnId($id);
				$item->setItemId($this->input->post('item_id'));
				$item->setItemDate(date('Y-m-d', strtotime( $purchase_data->date_purchase )));
				$item->setQuantity($this->input->post('quantity'));
				$item->setPrice($this->input->post('price'));
				$item->setType('in');
				if( $this->input->post('content') ) {
					$item->setContent($this->input->post('content'));
				}
				$item->insert();
				$new_item = $item->get_inserted_id();
			}
			$this->postNext();
			redirect( site_url("purchases/add_item/{$id}") . "?new_item={$new_item}");
		}

		if( $this->input->get('new_item') ) {
			$new_data = new $this->Inventory_stocks_model('is');
			$new_data->setId($this->input->get('new_item'),true);
			$new_data->set_join('products_items pi', 'pi.item_id=is.item_id');
			$this->template_data->set('new_item', $new_data->get());
		}

		if( $this->input->get('new_product') || $this->input->get('update_product') ) {
			$product = new $this->Products_items_model;
			$product_id = ($this->input->get('new_product')) ? $this->input->get('new_product') : $this->input->get('update_product');
			$product->setItemId($product_id,true);
			$this->template_data->set('new_product', $product->get());
		}

		$products = new $this->Products_items_model('pi');
		$products->setActive(1,true);
		$products->set_limit(0);
		$products->set_select("*");
		$products->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=pi.item_id AND si.type='in' ORDER BY si.id DESC LIMIT 1) as last_price");
		$products->set_order('pi.item_id', 'ASC');
		$this->template_data->set('items', $products->populate());

		$last_added = new $this->Inventory_stocks_model('i');
		$last_added->setConnId($id, true);
		$last_added->setType('in', true);
		$last_added->set_select("i.*");
		$last_added->set_join("products_items pi", 'pi.item_id=i.item_id');
		$last_added->set_select("pi.item_name as item_name");
		$last_added->set_select("pi.net_weight as net_weight");
		$last_added->set_order('i.id', 'DESC');
		$last_added->set_limit(10);
		$this->template_data->set('last_added', $last_added->populate());

		$this->load->view('purchases/purchases_add_item', $this->template_data->get_data());
	}

	public function edit_item($id, $output='') {
		$this->template_data->set('purchase_id', $id);
		$this->template_data->set('output', $output);

		$item = new $this->Inventory_stocks_model('is');
		$item->setId($id,true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('item_id', 'Item', 'trim|required');
			$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
			$this->form_validation->set_rules('price', 'Price per unit', 'trim|required');
			if( $this->form_validation->run() ) {
				$item->setItemId($this->input->post('item_id'),false,true);
				$item->setQuantity($this->input->post('quantity'),false,true);
				$item->setPrice($this->input->post('price'),false,true);
				if( $this->input->post('content') ) {
					$item->setContent($this->input->post('content'),false,true);
				}
				$item->update();
			}
			$this->postNext("new_item={$id}");
		}

		$this->template_data->set('current_item', $item->get());

		$products = new $this->Products_items_model('pi');
		$products->setActive(1,true);
		$products->set_limit(0);
		$products->set_select("*");
		$products->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=pi.item_id AND si.type=\"in\" ORDER BY si.price DESC LIMIT 1) as last_price");
		$products->set_order('pi.item_name', 'ASC');
		$this->template_data->set('items', $products->populate());

		$this->load->view('purchases/purchases_edit_item', $this->template_data->get_data());
	}

	public function delete_item($id) {

		$this->_isAuth('inventory', 'purchases', 'delete');

		$item = new $this->Inventory_stocks_model;
		$item->setId($id, true);
		$item->delete();

		redirect($this->input->get('next'));
	}

	public function analysis($output='') {

		$this->_isAuth('inventory', 'purchases', 'edit');
		$this->template_data->set('output', $output);


		$products = new $this->Products_items_model('pi');
		$products->setActive(1,true);
		$products->set_limit(0);
		$products->set_select("*");
		$products->set_select("(SELECT si.price FROM inventory_stocks si WHERE si.item_id=pi.item_id AND si.type='in' ORDER BY si.id DESC LIMIT 1) as last_price");
		$products->set_order('pi.item_name', 'ASC');
		$this->template_data->set('items', $products->populate());
		
		if( $output == 'ajax') {
			if( $this->input->post() ) {
				redirect( site_url("purchases/analysis") . "?start=" . $this->input->post('date_start') . "&end=" . $this->input->post('date_end') );
			}
			$this->load->view('purchases/purchases_analysis', $this->template_data->get_data());
		} else {

			if(($this->input->get('start')) && ($this->input->get('end'))  && ($this->input->get('item')) ) { 
				$stocks = new $this->Inventory_stocks_model('i');
				$stocks->setType('in', true);
				$stocks->setItemId($this->input->get('item'), true);
				$stocks->set_select("i.*");
				$stocks->set_join("products_items pi", 'pi.item_id=i.item_id');
				$stocks->set_select("pi.item_name as item_name");
				$stocks->set_select("pi.net_weight as net_weight");
				$stocks->set_order('i.id', 'DESC');
				$stocks->set_limit(0);
				$stocks->set_where('i.item_date >= "' .$this->input->get('start'). '"');
				//$stocks->set_where('i.item_date <= "' .$this->input->get('end'). '"');

				$this->template_data->set('stocks', $stocks->populate());
			}
			$this->load->view('purchases/purchases_analysis_data', $this->template_data->get_data());
		}
	}

}
