<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stocks extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Stocks');
		$this->template_data->set('current_uri', 'stocks');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('inventory', 'stocks', 'view');

		$this->template_data->set('alpha_uri', 'stocks');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("stocks?q=" . $this->input->get('q') ));
		}
	}

	private function _getStart($text) {
		$text = trim($text);
		$alpha = strtolower(substr($text, 0, 1));
		$alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
		$alpha = (in_array($alpha, $alphabet)) ? $alpha : '1';
		return $alpha;
	}

	public function index($start=0) {

		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$stocks = new $this->Inventory_stocks_model('is');
		$stocks->set_select('is.*');
		$stocks->set_group_by('is.item_id');
		$stocks->set_join('products_items pi', 'is.item_id=pi.item_id');
		$stocks->set_select('pi.item_name');
		$stocks->set_select('pi.net_weight');
		$stocks->set_select('pi.minimum');
		$stocks->set_join('products_category pc', 'pi.category_id=pc.id');
		$stocks->set_select('pc.id as category_id');
		$stocks->set_select('pc.name as category_name');
		$stocks->set_start($start);
		$stocks->set_limit(10);
		//$stocks->set_select('((IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in"),0)) - (IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out"),0))) as available_stocks');

		//$stocks->set_select('(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in") as in_total');
		//$stocks->set_select('(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out") as out_total');

		if($this->input->get('q'))  {
			if( (strlen($this->input->get('q')) >= 3) ) {
				$stocks->set_where('pi.item_name LIKE "%' . $this->input->get('q') . '%"');
				$stocks->set_where_or('pi.net_weight LIKE "%' . $this->input->get('q') . '%"');
				$stocks->set_limit(0);
				$this->template_data->set('all_stocks', $stocks->count_all_results());
				$this->template_data->set('stocks', $stocks->populate());
			} else {
				$this->template_data->set('all_stocks', 0);
				$this->template_data->set('stocks', false);
			}
		} else {
			$this->template_data->set('all_stocks', $stocks->count_all_results());
			$this->template_data->set('stocks', $stocks->populate());
		}

		

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/stocks/index/'),
			'total_rows' => $stocks->count_all_results(),
			'per_page' => $stocks->get_limit(),
			'ajax'=>true,
		)));

		

		$this->load->view('stocks/stocks_list', $this->template_data->get_data());
	}

	public function choose_date($output='') {

		if( $this->input->post() ) {
			$date_end = ($this->input->post("date_end")) ? date('Y-m-d', strtotime($this->input->post("date_end"))) : date('Y-m-d');
			redirect("stocks/below_minimum/{$date_end}");
		}

		$this->template_data->set('output', $output);
		$this->load->view('stocks/stocks_choose_date', $this->template_data->get_data());
	}

	public function below_minimum($date_end, $item_id=NULL) {

		$this->template_data->set('date_end', $date_end);

		$stocks = new $this->Inventory_stocks_model('is');
		if( $item_id ) {
			$stocks->setItemId($item_id,true);
			$this->template_data->set('item_id', $item_id);
		}
		$stocks->set_select('is.*');
		$stocks->set_group_by('is.item_id');
		$stocks->set_join('products_items pi', 'is.item_id=pi.item_id');
		$stocks->set_select('pi.item_name');
		$stocks->set_select('pi.net_weight');
		$stocks->set_select('pi.minimum');
		$stocks->set_select('pi.content');
		$stocks->set_join('products_category pc', 'pi.category_id=pc.id');
		$stocks->set_select('pc.id as category_id');
		$stocks->set_select('pc.name as category_name');
		$stocks->set_limit(0);
		$stocks->set_select('((IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in" AND item_date<="'.$date_end.'"),((SELECT SUM(quantity * (IF((content IS NOT NULL),content,pi.content)) ) FROM inventory_stocks WHERE item_id=is.item_id AND type="in" AND item_date<="'.$date_end.'")),0)) - (IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out" AND item_date<="'.$date_end.'"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out" AND item_date<="'.$date_end.'"),0))) as available_stocks');
if( !$item_id ) {
		$stocks->set_where('(((IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in" AND item_date<="'.$date_end.'"),((SELECT SUM(quantity * (IF((content IS NOT NULL),content,pi.content)) ) FROM inventory_stocks WHERE item_id=is.item_id AND type="in" AND item_date<="'.$date_end.'")),0)) - (IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out" AND item_date<="'.$date_end.'"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out" AND item_date<="'.$date_end.'"),0))) < pi.minimum)', NULL, 5);
}
if( $this->input->get('category') ) {
			$stocks->set_where("pi.category_id", $this->input->get('category'), NULL, 2);
			$this->template_data->set('category_id', $this->input->get('category'));
}
		$stocks->set_where("(pi.active=1)", NULL, 1);
		$stocks->set_where("(is.item_date <= '{$date_end}')", NULL, 0);

			$last_price = new $this->Inventory_stocks_model("si");
			$last_price->set_select("si.price");
			$last_price->set_where("si.item_id=is.item_id");
			$last_price->set_where("si.type LIKE 'in'");
			$last_price->set_order("si.price", "DESC");
			$last_price->set_limit(1);
			$stocks->set_select("(".$last_price->get_compiled_select().") as last_price");

		if($this->input->get('trolley')) {
			$stocks->set_where("pc.trolley", $this->input->get('trolley'));
		}

		$order_by = ($this->input->get('order_by')) ? $this->input->get('order_by') : 'ASC';
		switch($this->input->get('sort')) {
			case 'item_id':
				$stocks->set_order('pi.item_id', $order_by);
				break;
			case 'item_name':
				$stocks->set_order('pi.item_name', $order_by);
				$stocks->set_order('pi.item_id', $order_by);
				break;
			default:
			case 'category':
				$stocks->set_order('pc.name', $order_by);
				$stocks->set_order('pi.item_name', $order_by);	
				break;

		}
		$this->template_data->set('stocks', $stocks->populate());		

		$category = new $this->Products_category_model('pc');
		$category->set_group_by('pc.trolley',true);
		$category->set_order('pc.trolley', 'ASC');
		$category->set_select("pc.trolley", NULL, true);
		$this->template_data->set('trolley', $category->populate());

		$this->load->view('stocks/stocks_below_minimum', $this->template_data->get_data());
	}

	public function starts($alpha, $start=0) {

		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$this->template_data->set('alpha_start', $alpha);

		$stocks = new $this->Inventory_stocks_model('is');
		$stocks->set_select('is.*');
		$stocks->set_group_by('is.item_id');
		$stocks->set_join('products_items pi', 'is.item_id=pi.item_id');
		$stocks->set_select('pi.item_name');
		$stocks->set_select('pi.net_weight');
		$stocks->set_select('pi.minimum');
		$stocks->set_join('products_category pc', 'pi.category_id=pc.id');
		$stocks->set_select('pc.id as category_id');
		$stocks->set_select('pc.name as category_name');
		$stocks->set_start($start);

		$stocks->set_where('pi.start LIKE "'.$this->_getStart($alpha).'"');

		//$stocks->set_select('((IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in"),0)) - (IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out"),0))) as available_stocks');

		//$stocks->set_select('(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in") as in_total');
		//$stocks->set_select('(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out") as out_total');

		if( $this->input->get('q') ) {
			$stocks->set_where('pi.item_name LIKE "%' . $this->input->get('q') . '%"');
			$stocks->set_where_or('pi.net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$this->template_data->set('stocks', $stocks->populate());

		$this->template_data->set('all_stocks', $stocks->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/stocks/starts/' . $alpha),
			'total_rows' => $stocks->count_all_results(),
			'per_page' => $stocks->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('stocks/stocks_list', $this->template_data->get_data());
	}

	public function category($category_id, $start=0) {

		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$stocks = new $this->Inventory_stocks_model('is');
		$stocks->set_select('is.*');
		$stocks->set_group_by('is.item_id');
		$stocks->set_join('products_items pi', 'is.item_id=pi.item_id');
		$stocks->set_select('pi.item_name');
		$stocks->set_select('pi.net_weight');
		$stocks->set_select('pi.minimum');
		$stocks->set_join('products_category pc', 'pi.category_id=pc.id');
		$stocks->set_select('pc.id as category_id');
		$stocks->set_select('pc.name as category_name');
		$stocks->set_where("pi.category_id", $category_id);

		//$stocks->set_select('((IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="in"),0)) - (IF((SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out"),(SELECT SUM(quantity) FROM inventory_stocks WHERE item_id=is.item_id AND type="out"),0))) as available_stocks');

		if( $this->input->get('q') ) {
			$stocks->set_where('pi.item_name LIKE "%' . $this->input->get('q') . '%"');
			$stocks->set_where_or('pi.net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$stocks->set_start($start);
		
		$this->template_data->set('all_stocks', $stocks->count_all_results());
		$this->template_data->set('stocks', $stocks->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/stocks/index/'),
			'total_rows' => $stocks->count_all_results(),
			'per_page' => $stocks->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('stocks/stocks_list', $this->template_data->get_data());
	}

	public function entries($item_id, $start=0) {

		$date_end = $this->input->get('date_end');

		$limit = 10;

		$item = new $this->Products_items_model('i');
		$item->setItemId($item_id, true);
		$item->set_select("*");

		if( $start > 0 ) {
				$balance = new $this->Inventory_stocks_model("is");
				$balance->setItemId($item_id,true);
				$balance->set_select("is.type,is.quantity,is.content");
				$balance->set_start(0);
				$balance->set_limit($start);
				$balance->set_order('item_date', 'ASC');
				$balance_sql = $balance->get_compiled_select();
				$item->set_select("(SELECT SUM(balance.quantity * (IF((balance.content IS NOT NULL), balance.content, i.content))) FROM ({$balance_sql}) balance WHERE type='in') as in_balance");
				$item->set_select("(SELECT SUM(balance.quantity) FROM ({$balance_sql}) balance WHERE type='out') as out_balance");
		} else {
			$item->set_select("0 as in_balance");
			$item->set_select("0 as out_balance");
		}

		$this->template_data->set('item', $item->get());

		$stocks = new $this->Inventory_stocks_model('is');
		$stocks->setItemId($item_id,true);
		$stocks->set_select('is.*');
		$stocks->set_join('products_items pi', 'is.item_id=pi.item_id');
		$stocks->set_select('pi.item_name');
		$stocks->set_select('pi.net_weight');
		$stocks->set_select('(IF((is.content IS NOT NULL), is.content, pi.content)) as content');
		$stocks->set_start($start);
		$stocks->set_limit($limit);
		$stocks->set_order('is.item_date', 'ASC');
		$stocks->set_order('is.id', 'ASC');

		if( $date_end ) {
			$stocks->set_where("is.item_date <= '{$date_end}'");
		}

		$this->template_data->set('stocks', $stocks->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item("index_page") . '/stocks/entries/' . $item_id),
			'total_rows' => $stocks->count_all_results(),
			'per_page' => $stocks->get_limit(),
			'ajax'=>true,
		), include_any_querystrings()));

		$this->load->view('stocks/stocks_entries', $this->template_data->get_data());
	}

	public function analytics($item_id=NULL) {
		if( $item_id ) {
			$item = new $this->Products_items_model('i');
			$item->setItemId($item_id, true);
			$item->set_select("*");
			$this->template_data->set('item', $item->get());
		}

		$months = new $this->Inventory_stocks_model('is');
		if( $item_id ) {
			$months->setItemId($item_id,true);
		}
		$months->setType('out',true);
		$months->set_select('MONTH(is.item_date) as month');
		$months->set_select('DATE_FORMAT(is.item_date, "%M") as month_name');
		$months->set_limit(0);
		$months->set_group_by('MONTH(is.item_date)');
		$months->set_order('MONTH(is.item_date)', 'ASC');
		$months->set_where('(is.item_date IS NOT NULL)');
		$months->set_where('(is.item_date != "")');
		$months->set_where('(MONTH(is.item_date)!=0)');
		$this->template_data->set('months', $months->populate());

		$years = new $this->Inventory_stocks_model('is');
		if( $item_id ) {
			$years->setItemId($item_id,true);
		}
		$years->setType('out',true);
		$years->set_select('YEAR(is.item_date) as year');
		$years->set_limit(0);
		$years->set_group_by('YEAR(is.item_date)');
		$years->set_order('YEAR(is.item_date)', 'ASC');
		$years->set_where('(is.item_date IS NOT NULL)');
		$years->set_where('(is.item_date != "")');
		$years->set_where('(YEAR(is.item_date)!=0)');
		$this->template_data->set('years', $years->populate());

		if( $item_id ) {
			$total = new $this->Inventory_stocks_model('is');
			$total->setItemId($item_id,true);
			$total->setType('out',true);
			$total->set_select('SUM(is.quantity) as total');
			$total->set_select('MONTH(is.item_date) as month');
			$total->set_select('YEAR(is.item_date) as year');
			$total->set_limit(0);
			$total->set_group_by('YEAR(is.item_date), MONTH(is.item_date)');
			$total->set_where('(YEAR(is.item_date)!=0)');
			$total->set_where('(MONTH(is.item_date)!=0)');
			$this->template_data->set('total', $total->populate());

			$this->load->view('stocks/stocks_analytics_item', $this->template_data->get_data());
		} else {

			$items = new $this->Inventory_stocks_model('is');
			$items->set_select('is.*');
			$items->set_join('products_items pi', 'is.item_id=pi.item_id');
			$items->set_select('pi.item_name');
			$items->set_select('pi.net_weight');
			$items->set_limit(0);
			$items->set_order('pi.category_id', 'ASC');
			$items->set_join('products_category pc', 'pi.category_id=pc.id');
			$items->set_select('pc.name as category_name');
			$items->set_group_by('is.item_id');
			$this->template_data->set('items', $items->populate());

			$total = new $this->Inventory_stocks_model('is');
			$total->setType('out',true);
			$total->set_select('SUM(is.quantity) as total');
			$total->set_select('MONTH(is.item_date) as month');
			$total->set_select('YEAR(is.item_date) as year');
			$total->set_select('is.item_id');
			$total->set_limit(0);
			$total->set_group_by('is.item_id, YEAR(is.item_date), MONTH(is.item_date)');
			$total->set_where('(YEAR(is.item_date)!=0)');
			$total->set_where('(MONTH(is.item_date)!=0)');
			$total->set_where('(YEAR(is.item_date)='.date('Y').')');
			$this->template_data->set('total', $total->populate());

			$this->load->view('stocks/stocks_analytics_all', $this->template_data->get_data());
		}
	}

}
