<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Category');
		$this->template_data->set('current_uri', 'store');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('products', 'store', 'view');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("store?q=" . $this->input->get('q') ));
		}
	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$store = new $this->Products_store_model('i');

		if( $this->input->get('q') ) {
			$store->set_where('name LIKE "%' . $this->input->get('q') . '%"');
			$store->set_where_or('net_weight LIKE "%' . $this->input->get('q') . '%"');
		}

		$store->set_select("i.*");

		$store->set_order('i.name', 'ASC');
		$store->set_start($start);

		$this->template_data->set('stores', $store->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item("index_page") . '/store/index/'),
			'total_rows' => $store->count_all_results(),
			'per_page' => $store->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('store/store_list', $this->template_data->get_data());
	}


	public function add($output='') {

		$this->_isAuth('products', 'store', 'add');

		$this->template_data->set('output', $output);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Store Name', 'trim|required|is_unique[products_store.name]');
			if( $this->form_validation->run() ) {
				$store = new $this->Products_store_model;
				$store->setName($this->input->post('name'));
				$store->insert();
				$store_id = $store->get_inserted_id();
				redirect("store/edit/{$store_id}");
			}
			redirect("store");
		}
		$this->load->view('store/store_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('products', 'store', 'edit');

		$this->template_data->set('output', $output);

		$store = new $this->Products_store_model('i');
		$store->setId($id, true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('name', 'Store Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$store->setName($this->input->post('name'),false,true);
				$store->update();
			}
			$this->postNext();
		}

		$store->set_select("i.*");

		$this->template_data->set('item', $store->get());
		
		$this->load->view('store/store_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('products', 'store', 'delete');

		$store = new $this->Products_store_model;
		$store->setId($id, true);
		$store->delete();

		redirect( "store" );
	}

}
