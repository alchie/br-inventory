<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Inventory_stocks_model Class
 *
 * Manipulates `inventory_stocks` table on database

CREATE TABLE `inventory_stocks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `item_id` int(20) NOT NULL,
  `conn_id` int(20) NOT NULL,
  `type` varchar(3) DEFAULT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `price` decimal(20,5) NOT NULL DEFAULT '0.00000',
  `receipt_id` int(20) DEFAULT NULL,
  `item_date` date NOT NULL,
  `content` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `inventory_stocks` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `inventory_stocks` ADD  `item_id` int(20) NOT NULL   ;
 ALTER TABLE  `inventory_stocks` ADD  `conn_id` int(20) NOT NULL   ;
 ALTER TABLE  `inventory_stocks` ADD  `type` varchar(3) NULL   ;
 ALTER TABLE  `inventory_stocks` ADD  `quantity` int(10) NOT NULL   DEFAULT '0';
 ALTER TABLE  `inventory_stocks` ADD  `price` decimal(20,5) NOT NULL   DEFAULT '0.00000';
 ALTER TABLE  `inventory_stocks` ADD  `receipt_id` int(20) NULL   ;
 ALTER TABLE  `inventory_stocks` ADD  `item_date` date NOT NULL   ;
 ALTER TABLE  `inventory_stocks` ADD  `content` int(11) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Inventory_stocks_model extends MY_Model {

	protected $id;
	protected $item_id;
	protected $conn_id;
	protected $type;
	protected $quantity;
	protected $price;
	protected $receipt_id;
	protected $item_date;
	protected $content;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'inventory_stocks';
		$this->_short_name = 'inventory_stocks';
		$this->_fields = array("id","item_id","conn_id","type","quantity","price","receipt_id","item_date","content");
		$this->_required = array("item_id","conn_id","quantity","price","item_date");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: item_id -------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	*/

		public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_id` variable
	* @access public
	*/

		public function getItemId() {
			return $this->item_id;
		}
	
// ------------------------------ End Field: item_id --------------------------------------


// ---------------------------- Start Field: conn_id -------------------------------------- 

	/** 
	* Sets a value to `conn_id` variable
	* @access public
	*/

		public function setConnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('conn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `conn_id` variable
	* @access public
	*/

		public function getConnId() {
			return $this->conn_id;
		}
	
// ------------------------------ End Field: conn_id --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: quantity -------------------------------------- 

	/** 
	* Sets a value to `quantity` variable
	* @access public
	*/

		public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `quantity` variable
	* @access public
	*/

		public function getQuantity() {
			return $this->quantity;
		}
	
// ------------------------------ End Field: quantity --------------------------------------


// ---------------------------- Start Field: price -------------------------------------- 

	/** 
	* Sets a value to `price` variable
	* @access public
	*/

		public function setPrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('price', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `price` variable
	* @access public
	*/

		public function getPrice() {
			return $this->price;
		}
	
// ------------------------------ End Field: price --------------------------------------


// ---------------------------- Start Field: receipt_id -------------------------------------- 

	/** 
	* Sets a value to `receipt_id` variable
	* @access public
	*/

		public function setReceiptId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('receipt_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `receipt_id` variable
	* @access public
	*/

		public function getReceiptId() {
			return $this->receipt_id;
		}
	
// ------------------------------ End Field: receipt_id --------------------------------------


// ---------------------------- Start Field: item_date -------------------------------------- 

	/** 
	* Sets a value to `item_date` variable
	* @access public
	*/

		public function setItemDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_date` variable
	* @access public
	*/

		public function getItemDate() {
			return $this->item_date;
		}
	
// ------------------------------ End Field: item_date --------------------------------------


// ---------------------------- Start Field: content -------------------------------------- 

	/** 
	* Sets a value to `content` variable
	* @access public
	*/

		public function setContent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('content', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `content` variable
	* @access public
	*/

		public function getContent() {
			return $this->content;
		}
	
// ------------------------------ End Field: content --------------------------------------




}

/* End of file Inventory_stocks_model.php */
/* Location: ./application/models/Inventory_stocks_model.php */
