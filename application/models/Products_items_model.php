<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Products_items_model Class
 *
 * Manipulates `products_items` table on database

CREATE TABLE `products_items` (
  `item_id` int(20) NOT NULL AUTO_INCREMENT,
  `start` varchar(1) NOT NULL DEFAULT '1',
  `item_name` varchar(200) NOT NULL,
  `net_weight` varchar(200) DEFAULT NULL,
  `content` int(10) NOT NULL DEFAULT '1',
  `category_id` int(20) DEFAULT NULL,
  `store_id` int(20) DEFAULT '0',
  `minimum` int(20) DEFAULT '1',
  `active` int(1) DEFAULT '1',
  `shelf` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
);

 ALTER TABLE  `products_items` ADD  `item_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `products_items` ADD  `start` varchar(1) NOT NULL   DEFAULT '1';
 ALTER TABLE  `products_items` ADD  `item_name` varchar(200) NOT NULL   ;
 ALTER TABLE  `products_items` ADD  `net_weight` varchar(200) NULL   ;
 ALTER TABLE  `products_items` ADD  `content` int(10) NOT NULL   DEFAULT '1';
 ALTER TABLE  `products_items` ADD  `category_id` int(20) NULL   ;
 ALTER TABLE  `products_items` ADD  `store_id` int(20) NULL   DEFAULT '0';
 ALTER TABLE  `products_items` ADD  `minimum` int(20) NULL   DEFAULT '1';
 ALTER TABLE  `products_items` ADD  `active` int(1) NULL   DEFAULT '1';
 ALTER TABLE  `products_items` ADD  `shelf` int(11) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Products_items_model extends MY_Model {

	protected $item_id;
	protected $start;
	protected $item_name;
	protected $net_weight;
	protected $content;
	protected $category_id;
	protected $store_id;
	protected $minimum;
	protected $active;
	protected $shelf;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'products_items';
		$this->_short_name = 'products_items';
		$this->_fields = array("item_id","start","item_name","net_weight","content","category_id","store_id","minimum","active","shelf");
		$this->_required = array("start","item_name","content");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: item_id -------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	*/

		public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_id` variable
	* @access public
	*/

		public function getItemId() {
			return $this->item_id;
		}
	
// ------------------------------ End Field: item_id --------------------------------------


// ---------------------------- Start Field: start -------------------------------------- 

	/** 
	* Sets a value to `start` variable
	* @access public
	*/

		public function setStart($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('start', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `start` variable
	* @access public
	*/

		public function getStart() {
			return $this->start;
		}
	
// ------------------------------ End Field: start --------------------------------------


// ---------------------------- Start Field: item_name -------------------------------------- 

	/** 
	* Sets a value to `item_name` variable
	* @access public
	*/

		public function setItemName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_name` variable
	* @access public
	*/

		public function getItemName() {
			return $this->item_name;
		}
	
// ------------------------------ End Field: item_name --------------------------------------


// ---------------------------- Start Field: net_weight -------------------------------------- 

	/** 
	* Sets a value to `net_weight` variable
	* @access public
	*/

		public function setNetWeight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('net_weight', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `net_weight` variable
	* @access public
	*/

		public function getNetWeight() {
			return $this->net_weight;
		}
	
// ------------------------------ End Field: net_weight --------------------------------------


// ---------------------------- Start Field: content -------------------------------------- 

	/** 
	* Sets a value to `content` variable
	* @access public
	*/

		public function setContent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('content', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `content` variable
	* @access public
	*/

		public function getContent() {
			return $this->content;
		}
	
// ------------------------------ End Field: content --------------------------------------


// ---------------------------- Start Field: category_id -------------------------------------- 

	/** 
	* Sets a value to `category_id` variable
	* @access public
	*/

		public function setCategoryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('category_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `category_id` variable
	* @access public
	*/

		public function getCategoryId() {
			return $this->category_id;
		}
	
// ------------------------------ End Field: category_id --------------------------------------


// ---------------------------- Start Field: store_id -------------------------------------- 

	/** 
	* Sets a value to `store_id` variable
	* @access public
	*/

		public function setStoreId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('store_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `store_id` variable
	* @access public
	*/

		public function getStoreId() {
			return $this->store_id;
		}
	
// ------------------------------ End Field: store_id --------------------------------------


// ---------------------------- Start Field: minimum -------------------------------------- 

	/** 
	* Sets a value to `minimum` variable
	* @access public
	*/

		public function setMinimum($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('minimum', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `minimum` variable
	* @access public
	*/

		public function getMinimum() {
			return $this->minimum;
		}
	
// ------------------------------ End Field: minimum --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: shelf -------------------------------------- 

	/** 
	* Sets a value to `shelf` variable
	* @access public
	*/

		public function setShelf($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('shelf', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `shelf` variable
	* @access public
	*/

		public function getShelf() {
			return $this->shelf;
		}
	
// ------------------------------ End Field: shelf --------------------------------------




}

/* End of file Products_items_model.php */
/* Location: ./application/models/Products_items_model.php */
