-- Table structure for table `account_sessions` 

CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `inventory_orders` 

CREATE TABLE `inventory_orders` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `date_order` datetime NOT NULL,
  `requester` varchar(200) DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`id`)
);

-- Table structure for table `inventory_purchases` 

CREATE TABLE `inventory_purchases` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(20) DEFAULT NULL,
  `date_purchase` date NOT NULL,
  `purchaser` varchar(200) DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`id`)
);

-- Table structure for table `inventory_stocks` 

CREATE TABLE `inventory_stocks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `item_id` int(20) NOT NULL,
  `conn_id` int(20) NOT NULL,
  `type` varchar(3) DEFAULT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `price` decimal(20,5) NOT NULL DEFAULT '0.00000',
  `receipt_id` int(20) DEFAULT NULL,
  `item_date` date NOT NULL,
  `content` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `products_category` 

CREATE TABLE `products_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `trolley` int(2) DEFAULT '1',
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `products_items` 

CREATE TABLE `products_items` (
  `item_id` int(20) NOT NULL AUTO_INCREMENT,
  `start` varchar(1) NOT NULL DEFAULT '1',
  `item_name` varchar(200) NOT NULL,
  `net_weight` varchar(200) DEFAULT NULL,
  `content` int(10) NOT NULL DEFAULT '1',
  `category_id` int(20) DEFAULT NULL,
  `store_id` int(20) DEFAULT '0',
  `minimum` int(20) DEFAULT '1',
  `active` int(1) DEFAULT '1',
  `shelf` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
);

-- Table structure for table `products_store` 

CREATE TABLE `products_store` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `user_accounts` 

CREATE TABLE `user_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
);

-- Table structure for table `user_accounts_restrictions` 

CREATE TABLE `user_accounts_restrictions` (
  `uid` int(20) NOT NULL,
  `department` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `view` int(1) NOT NULL DEFAULT '0',
  `add` int(1) NOT NULL DEFAULT '0',
  `edit` int(1) NOT NULL DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '0',
  KEY `uid` (`uid`)
);

