<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Inventory_purchases_model Class
 *
 * Manipulates `inventory_purchases` table on database

CREATE TABLE `inventory_purchases` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(20) DEFAULT NULL,
  `date_purchase` date NOT NULL,
  `purchaser` varchar(200) DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `inventory_purchases` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `inventory_purchases` ADD  `order_id` int(20) NULL   ;
 ALTER TABLE  `inventory_purchases` ADD  `date_purchase` date NOT NULL   ;
 ALTER TABLE  `inventory_purchases` ADD  `purchaser` varchar(200) NULL   ;
 ALTER TABLE  `inventory_purchases` ADD  `remarks` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Inventory_purchases_model extends MY_Model {

	protected $id;
	protected $order_id;
	protected $date_purchase;
	protected $purchaser;
	protected $remarks;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'inventory_purchases';
		$this->_short_name = 'inventory_purchases';
		$this->_fields = array("id","order_id","date_purchase","purchaser","remarks");
		$this->_required = array("date_purchase");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: order_id -------------------------------------- 

	/** 
	* Sets a value to `order_id` variable
	* @access public
	*/

		public function setOrderId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('order_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `order_id` variable
	* @access public
	*/

		public function getOrderId() {
			return $this->order_id;
		}
	
// ------------------------------ End Field: order_id --------------------------------------


// ---------------------------- Start Field: date_purchase -------------------------------------- 

	/** 
	* Sets a value to `date_purchase` variable
	* @access public
	*/

		public function setDatePurchase($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date_purchase', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date_purchase` variable
	* @access public
	*/

		public function getDatePurchase() {
			return $this->date_purchase;
		}
	
// ------------------------------ End Field: date_purchase --------------------------------------


// ---------------------------- Start Field: purchaser -------------------------------------- 

	/** 
	* Sets a value to `purchaser` variable
	* @access public
	*/

		public function setPurchaser($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('purchaser', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `purchaser` variable
	* @access public
	*/

		public function getPurchaser() {
			return $this->purchaser;
		}
	
// ------------------------------ End Field: purchaser --------------------------------------


// ---------------------------- Start Field: remarks -------------------------------------- 

	/** 
	* Sets a value to `remarks` variable
	* @access public
	*/

		public function setRemarks($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('remarks', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `remarks` variable
	* @access public
	*/

		public function getRemarks() {
			return $this->remarks;
		}
	
// ------------------------------ End Field: remarks --------------------------------------




}

/* End of file Inventory_purchases_model.php */
/* Location: ./application/models/Inventory_purchases_model.php */
