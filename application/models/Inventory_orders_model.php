<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Inventory_orders_model Class
 *
 * Manipulates `inventory_orders` table on database

CREATE TABLE `inventory_orders` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `date_order` datetime NOT NULL,
  `requester` varchar(200) DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `inventory_orders` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `inventory_orders` ADD  `date_order` datetime NOT NULL   ;
 ALTER TABLE  `inventory_orders` ADD  `requester` varchar(200) NULL   ;
 ALTER TABLE  `inventory_orders` ADD  `remarks` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Inventory_orders_model extends MY_Model {

	protected $id;
	protected $date_order;
	protected $requester;
	protected $remarks;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'inventory_orders';
		$this->_short_name = 'inventory_orders';
		$this->_fields = array("id","date_order","requester","remarks");
		$this->_required = array("date_order");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: date_order -------------------------------------- 

	/** 
	* Sets a value to `date_order` variable
	* @access public
	*/

		public function setDateOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date_order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date_order` variable
	* @access public
	*/

		public function getDateOrder() {
			return $this->date_order;
		}
	
// ------------------------------ End Field: date_order --------------------------------------


// ---------------------------- Start Field: requester -------------------------------------- 

	/** 
	* Sets a value to `requester` variable
	* @access public
	*/

		public function setRequester($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('requester', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `requester` variable
	* @access public
	*/

		public function getRequester() {
			return $this->requester;
		}
	
// ------------------------------ End Field: requester --------------------------------------


// ---------------------------- Start Field: remarks -------------------------------------- 

	/** 
	* Sets a value to `remarks` variable
	* @access public
	*/

		public function setRemarks($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('remarks', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `remarks` variable
	* @access public
	*/

		public function getRemarks() {
			return $this->remarks;
		}
	
// ------------------------------ End Field: remarks --------------------------------------




}

/* End of file Inventory_orders_model.php */
/* Location: ./application/models/Inventory_orders_model.php */
