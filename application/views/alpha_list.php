<?php $uri = (isset($alpha_uri)) ? $alpha_uri : 'items'; ?>
<div class="row hidden-print">

    		<div class="col-md-6 col-md-offset-3">
    			<div class="panel panel-default">
    				<div class="panel-body">
                    <form method="get" action="<?php echo site_url($uri); ?>">
<div class="input-group">
      <input type="text" class="form-control" placeholder="Search for..." name="q" value="<?php echo $this->input->get('q'); ?>">
      <span class="input-group-btn">
        <button class="btn btn-info" type="submit"><span class="glyphicon glyphicon-search"></span></button>
      </span>
    </div><!-- /input-group -->
    </form>
					</div>
    			</div>
    		</div>

</div>

<div class="form-group text-center hidden-print">
    <div class="btn-group">
    <a class="btn btn-<?php echo (!isset($alpha_start)) ? 'primary' : 'default'; ?>" href="<?php echo site_url($uri); ?>">All Items</a>
        <?php foreach(array(
'1' => '#',
'a' => 'A',
'b' => 'B',
'b' => 'B',
'c' => 'c',
'd' => 'd',
'e' => 'e',
'f' => 'f',
'g' => 'g',
'h' => 'h',
'i' => 'i',
'j' => 'j',
'k' => 'k',
'l' => 'l',
'm' => 'm',
'n' => 'n',
'o' => 'o',
'p' => 'p',
'q' => 'q',
'r' => 'r',
's' => 's',
't' => 't',
'u' => 'u',
'v' => 'v',
'w' => 'w',
'x' => 'x',
'y' => 'y',
'z' => 'Z',
        ) as $key=>$value) { ?>
            <a class="btn btn-<?php echo (isset($alpha_start)&&($alpha_start==$key)) ? 'primary' : 'default'; ?>" href="<?php echo site_url("{$uri}/starts/{$key}"); ?>"><?php echo strtoupper($value); ?></a>
        <?php } ?>
    </div>
</div>