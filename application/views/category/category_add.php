<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Category</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
	    		<div class="form-group">
	    			<label>Category Name</label>
	    			<input name="name" type="text" class="form-control" value="<?php echo $this->input->post('item_name'); ?>">
	    		</div>

	    		<div class="form-group">
	    			<label>Trolley Number</label>
	    			<input name="trolley" type="text" class="form-control" value="<?php echo ($this->input->post('trolley')) ? $this->input->post('trolley') : 1; ?>">
	    		</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("category"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>