<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('products', 'category', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Category" data-url="<?php echo site_url("category/add/ajax") . "?next=" . uri_string(); ?>">Add Category</button>
<?php } ?>
	    		<h3 class="panel-title">Category List</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $categories ) { ?>
	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Category Name</th>
	    					<th>Trolley Number</th>
	    					<?php if( hasAccess('products', 'category', 'edit') ) { ?>
	    					<th width="50px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($categories as $category) { ?>
	    				<tr>
	    					<td><?php echo $category->name; ?></td>
	    					<td><?php echo $category->trolley; ?></td>
	    					<?php if( hasAccess('products', 'category', 'edit') ) { ?>
	    					<td>

	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Category" data-url="<?php echo site_url("category/edit/{$category->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button>


	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>