<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('alpha_list'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<a class="ajax-modal hidden-print pull-right" data-toggle="modal" href="#ajaxModal" data-title="Daily Report" data-url="<?php echo site_url("orders/choose_dates/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-calendar"></span></a>

<a class="ajax-modal hidden-print pull-right" data-toggle="modal" href="#ajaxModal" data-title="Below Minimum" data-url="<?php echo site_url("stocks/choose_date/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 10px;"><span class="glyphicon glyphicon-shopping-cart"></span></a>

	    		<h3 class="panel-title">Stocks List (<strong><?php echo $all_stocks; ?> Item<?php echo ($all_stocks>1) ? 's':''; ?></strong>)</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $stocks ) { ?>
	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Item name</th>
	    					<th>Net Weight</th>
	    					<th>Category</th>
	    					<th>Minimum</th>
	    					<th class="text-right"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($stocks as $stock) { ?>
	    				<tr class="">
	    					<td>
	    					<?php echo $stock->item_name; ?>
	    					<?php if( hasAccess('products', 'items', 'edit') ) { ?>
	    					<a class="ajax-modal" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("items/edit/{$stock->item_id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
	    					<?php } ?>
	    					</td>
	    					<td><?php echo $stock->net_weight; ?></td>
	    					<td>
							<a href="<?php echo site_url("stocks/category/{$stock->category_id}"); ?>"><?php echo $stock->category_name; ?></a>
	    					</td>
	    					<td><?php echo $stock->minimum; ?></td>
	    					<td class="text-right">
	    					<a class="btn btn-success btn-xs" href="<?php echo site_url("stocks/entries/{$stock->item_id}"); ?>">
	    					Entries
	    						</a>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>