<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Print Below Minimum</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">

<?php if($trolley) { ?>
<center class="hidden_print" style="font-size: 14px;font-weight: bold">
<?php foreach($trolley as $troll) { ?>
	<a <?php echo ($this->input->get('trolley')==$troll->trolley) ? 'style="font-size:20px;"' : ''; ?> href="<?php echo ($this->input->get('trolley')==$troll->trolley) ? '#trolley' . $troll->trolley : site_url(uri_string()) . append_querystring("trolley", $troll->trolley); ?>"><?php echo $troll->trolley; ?></a>
<?php } ?>
<?php if($this->input->get('trolley')) { ?>
	<a href="<?php echo site_url(uri_string()) . append_querystring("trolley", 0); ?>">Show All</a>
<?php }  ?>
</center>
<?php } ?>

	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">ID <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'item_id','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
<?php if($this->input->get('trolley')) { ?>
	    					<th class="text-center">Quantity</th>
<?php } ?>
	    					<th>Item name <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'item_name','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a>
	    					</th>
	    					<th class="text-center">Category <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'category','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
<?php if(!$this->input->get('trolley')) { ?>
	    					<th class="text-right">Minimum</th>
	    					<th class="text-right">Stocks</th>
	    					<th class="text-right">Reduced</th>
	    					<th class="text-right">To Buy</th>
<?php } ?>
	    					<th class="text-right">Price</th>
	    					<th class="text-right">Total Amount</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$grand_total = 0;
	    			foreach($stocks as $item) { ?>
	    				<tr>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
<?php if($this->input->get('trolley')) { ?>
	    					<td class="text-center"><?php echo ceil(($item->minimum - $item->available_stocks) / $item->content); ?></td>
<?php } ?>
	    					<td><?php echo $item->item_name; ?> (<?php echo $item->net_weight; ?>)
<?php if( isset($item_id) ) { ?>
	<a href="<?php echo site_url("stocks/below_minimum/{$date_end}"); ?>" class="hidden_print"><span class="fa fa-close"></span></a>
<?php } else { ?>
	    					<a href="<?php echo site_url("stocks/below_minimum/{$date_end}/{$item->item_id}"); ?>" class="hidden_print"><span class="fa fa-filter"></span></a>
<?php } ?>

<a href="<?php echo site_url("items/edit/{$item->item_id}"); ?>" class="hidden_print" target="_edit_item"><span class="fa fa-pencil"></span></a>

	    					</td>
	    					<td class="text-center"><?php echo $item->category_name; ?>
<?php if( $item->category_id ) { ?> 
<?php if( isset($category_id) ) { ?>
	<a href="<?php echo site_url("stocks/below_minimum/{$date_end}"); ?>" class="hidden_print"><span class="fa fa-close"></span></a>
<?php } else { ?>
	    					<a href="<?php echo site_url("stocks/below_minimum/{$date_end}"); ?>?category=<?php echo $item->category_id; ?>" class="hidden_print"><span class="fa fa-filter"></span></a>
<?php } ?>
<?php } ?>
	    					</td>
<?php if(!$this->input->get('trolley')) { ?>
	    					<td class="text-right"><?php echo $item->minimum; ?></td>
	    					<td class="text-right"><a href="<?php echo site_url("stocks/entries/{$item->item_id}") . "?date_end=" . $date_end; ?>" class="hidden_print" target="_stocks_entries"><?php echo $item->available_stocks; ?></a></td>
	    					<td class="text-right"><?php echo ($item->minimum - $item->available_stocks); ?></td>
	    					<td class="text-right"><?php echo ceil(($item->minimum - $item->available_stocks) / $item->content); ?></td>
<?php } ?>
	    					<td class="text-right"><?php echo number_format($item->last_price,2); ?></td>
	    					<td class="text-right"><?php  
$total_amount = ((ceil(($item->minimum - $item->available_stocks) / $item->content)) * $item->last_price);
$grand_total += $total_amount;
	    					echo number_format($total_amount,2); ?></td>
	    				</tr>
	    			<?php } ?>
	    			<tr>
	    				<td colspan="<?php echo ($this->input->get('trolley')) ? '5' : '8'; ?>" class="text-right bold">GRAND TOTAL</td>
	    				<td class="text-right bold"><?php echo number_format($grand_total,2); ?></td>
	    			</tr>
	    			</tbody>
	    		</table>
<?php if( isset($item_id) || isset($category_id) ) { ?>
	<center><a href="<?php echo site_url("stocks/below_minimum/{$date_end}"); ?>" class="hidden_print"><h3>Show All</h3></a></center>
<?php } ?>
  </body>
</html>
