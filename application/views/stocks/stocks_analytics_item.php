<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( isset($item) ) { ?>
	    	<a class="pull-right" href="<?php echo site_url("stocks/entries/{$item->item_id}"); ?>"><span class="glyphicon glyphicon-list"></span></a>
	    		<h3 class="panel-title"><?php echo $item->item_name; ?></h3>
<?php } else { ?>
	<h3 class="panel-title">All Items</h3>
<?php } ?>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>YEAR</th>
	    					<?php foreach($months as $month) { ?>
	    						<th class="text-center"><?php echo $month->month_name; ?></th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($years as $year) { ?>
	    				<tr>	
	    						<td><?php echo $year->year; ?></td>
	    						<?php foreach($months as $month) { ?>
	    							<td class="text-center">
	    								<?php foreach($total as $tot) {
	    									if(($tot->month==$month->month) && ($tot->year==$year->year)) {
	    										echo $tot->total;
	    									}
	    								} ?>
	    							</td>
	    						<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>


<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>