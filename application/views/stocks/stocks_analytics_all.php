<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( isset($item) ) { ?>
	    	<a class="pull-right" href="<?php echo site_url("stocks/entries/{$item->item_id}"); ?>"><span class="glyphicon glyphicon-list"></span></a>
	    		<h3 class="panel-title"><?php echo $item->item_name; ?></h3>
<?php } else { ?>
	<h3 class="panel-title">All Items</h3>
<?php } ?>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th width="1%">Item ID</th>
	    					<th>Item Name</th>
	    					<th>Category</th>
	    					<?php foreach($months as $month) { ?>
	    						<th class="text-center"><?php echo $month->month_name; ?></th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($items as $item) { ?>
	    				<tr>	
	    						<td><?php echo $item->item_id; ?></td>
	    						<td><?php echo $item->item_name; ?> (<?php echo $item->net_weight; ?>)</td>
	    						<td><?php echo $item->category_name; ?></td>
	    						<?php foreach($months as $month) { ?>
	    							<td class="text-center">
	    								<?php foreach($total as $tot) {
	    									if(($tot->item_id==$item->item_id) && ($tot->month==$month->month)) {
	    										echo $tot->total;
	    									}
	    								} ?>
	    							</td>
	    						<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>


<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>