<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<a class="pull-right" href="<?php echo site_url("stocks/analytics/{$item->item_id}"); ?>"><span class="glyphicon glyphicon-signal"></span></a>
	    		<h3 class="panel-title"><?php echo $item->item_name; ?> <a class="ajax-modal" data-toggle="modal" href="#ajaxModal" data-title="Edit item" data-url="<?php echo site_url("items/edit/{$item->item_id}/ajax") . "?next=" . uri_string(); ?>"><span class="fa fa-pencil"></span></a></h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $stocks ) { 
$balance = ($item->in_balance - $item->out_balance);
$balance_total = 0;
	?>
	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Item name</th>
	    					<th>Net Weight</th>
	    					<th>Price</th>
	    					<th class="text-right">In</th>
	    					<th class="text-right">Out</th>
	    					<th class="text-right">Balance</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<tr class="warning">
	    			<td colspan="5"><strong>Beginning Balance / Balance Forwarded</strong></td>
	    					<td class="text-right"><strong><?php echo $balance; ?></strong>
<br><small><?php echo number_format($balance_total,2); ?></small>
	    					</td>
	    			<?php 
	    			foreach($stocks as $stock) { ?>
	    				<tr>
	    					<td><?php echo date("F d, Y", strtotime($stock->item_date)); ?></td>
	    					<td><?php echo $stock->net_weight; ?></td>
	    					<td><?php echo number_format($stock->price,2); ?></td>
<?php if( $stock->type == 'in') { 
	$balance+=($stock->quantity * $stock->content); 
	$balance_total+=($stock->price * $stock->quantity); 
	?>
	    					<td class="text-right">
	    					<a class="ajax-modal" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("purchases/edit_item/{$stock->id}/ajax") . "?next=" . uri_string(); ?>"><?php echo ($stock->quantity * $stock->content); ?></a>
<br><small><?php echo number_format(($stock->price * $stock->quantity),2); ?></small>
	    					</td>
	    					<td class="text-right"></td>
<?php } else { 
	$balance-=$stock->quantity; 
	$balance_total-=($stock->price * $stock->quantity); 
	?>
	    					<td class="text-right"></td>
	    					<td class="text-right">
	    					<a class="ajax-modal" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("orders/edit_item/{$stock->id}/ajax") . "?next=" . uri_string(); ?>"><?php echo $stock->quantity; ?></a>
	<br><small><?php echo number_format(($stock->price * $stock->quantity),2); ?></small>
	    					</td>
<?php } ?>
	    					<td class="text-right"><strong><?php echo $balance; ?></strong>
<br><small><?php echo number_format($balance_total,2); ?></small>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>