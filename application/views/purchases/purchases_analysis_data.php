<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<div class="btn-group pull-right">
  <button type="button" class="btn btn-danger btn-xs">Change Item</button>
  <button type="button" class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
  	<?php foreach($items as $item) { ?>
    <li><a href="<?php echo site_url("purchases/analysis") . '?start='. $this->input->get('start') . '&end=' . $this->input->get('end') . '&item=' . $item->item_id; ?>"><?php echo $item->item_name; ?></a></li>
    <?php } ?>
  </ul>
</div>

	    		<h3 class="panel-title">Purchases Analysis</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif;  ?>
<?php if( isset($stocks) && ($stocks) ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th width="100px">Date</th>
	    					<th width="20px">ItemID</th>
	    					<th>ItemName</th>
	    					<th class="text-right">Quantity</th>
	    					<th class="text-right">PricePerUnit</th>
	    					<th class="text-right">TotalPrice</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$total = 0;
	    			foreach($stocks as $stock) { ?>
	    				<tr>
	    					<td><?php echo $stock->item_date; ?></td>
	    					<td><?php echo $stock->item_id; ?></td>
	    					<td><?php echo $stock->item_name; ?> (<?php echo $stock->net_weight; ?>)</td>
	    					<td class="text-right"><?php echo $stock->quantity; ?></td>
	    					<td class="text-right"><?php echo number_format($stock->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($stock->price * $stock->quantity);
	    					echo number_format(($stock->price * $stock->quantity),2); 
	    					?></td>
	    				</tr>
	    			<?php } ?>
						<tr>
	    					<td class="text-right"></td>
	    					<td>TOTAL</td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"><strong><?php echo number_format($total,2); ?></strong></td>
	    				</tr>
	    			</tbody>
	    		</table>


<?php } else { ?>
	<div class="text-center">No item Found!</div>

<center>
<!-- Split button -->
<div class="btn-group">
  <button type="button" class="btn btn-danger">Select an Item</button>
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
  	<?php foreach($items as $item) { ?>
    <li><a href="<?php echo site_url("purchases/analysis") . '?start='. $this->input->get('start') . '&end=' . $this->input->get('end') . '&item=' . $item->item_id; ?>"><?php echo $item->item_name; ?></a></li>
    <?php } ?>
  </ul>
</div>
</center>

<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>