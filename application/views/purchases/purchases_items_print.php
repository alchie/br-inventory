<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Print</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">
<div class="header-label">
<h1>Bishop's Residence</h1>
<h2>Purchase Report</h2>
<h3><?php echo date("F d, Y", strtotime($purchase->date_purchase)); ?></h3>
</div>
	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">ID</th>
	    					<th>Item name</th>
	    					<th class="text-center">Net Weight</th>
	    					<th class="text-center">Category</th>
	    					<th class="text-center">Quantity</th>
	    					<th class="text-center">Price per Unit</th>
	    					<th class="text-center">Total</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php $total = 0; 
	    			foreach($stocks as $item) { ?>
	    				<tr>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
	    					<td><?php echo $item->item_name; ?></td>
	    					<td class="text-center"><?php echo $item->net_weight; ?></td>
	    					<td class="text-center"><?php echo $item->category_name; ?></td>
	    					<td class="text-center"><?php echo $item->quantity; ?></td>
	    					<td class="text-center"><?php echo number_format($item->price,2); ?></td>
	    					<td class="text-right"><?php $total += ($item->price * $item->quantity); echo number_format(($item->price * $item->quantity),2); ?></td>
	    				</tr>
	    			<?php } ?>
						<tr>
	    					<td  colspan="6" class="text-right bold highlight_td">TOTAL</td>
	    					<td class="text-right bold highlight_td"><?php echo number_format($total,2); ?></td>
	    				</tr>
	    			</tbody>
	    		</table>

<h3 style="margin-top:40px">Purchased by: <?php echo $purchase->purchaser; ?></h3>
  </body>
</html>
