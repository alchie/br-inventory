<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<?php if( hasAccess('inventory', 'purchases', 'add') ) { ?>
 <a class="btn btn-success btn-xs pull-right hidden-print" href="<?php echo site_url("purchases/add_item/{$purchase_id}"); ?>">Add Item</a>
<?php } ?>
<?php if( $purchase->order_id ) { ?>
<a class="ajax-modal btn btn-info btn-xs pull-right hidden-print" href="<?php echo site_url("orders/items/{$purchase->order_id}"); ?>" style="margin-right: 5px">Order</a>
<?php } ?>
<a class="hidden-print pull-right" href="<?php echo site_url("purchases/items_print/{$purchase_id}"); ?>" target='_blank' style="margin-right: 10px;"><span class="glyphicon glyphicon-print"></span></a>

	    		<h3 class="panel-title">Purchased Items - <strong><?php echo date('m/d/Y', strtotime($purchase->date_purchase)); ?> &middot; <?php echo $purchase->purchaser; ?></strong>
<a class="ajax-modal hidden-print" data-toggle="modal" href="#ajaxModal" data-title="Edit Purchase" data-url="<?php echo site_url("purchases/edit/{$purchase->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>

	    		</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>

<?php if( $stocks ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th width="20px">ItemID</th>
	    					<th>ItemName</th>
	    					<th class="text-right">Quantity</th>
	    					<th class="text-right">PricePerUnit</th>
	    					<th class="text-right">TotalPrice</th>
	    					<?php if( hasAccess('inventory', 'purchases', 'edit') ) { ?>
	    					<th width="120px" class="hidden-print">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$total = 0;
	    			foreach($stocks as $stock) { ?>
	    				<tr>
	    					<td><?php echo $stock->item_id; ?></td>
	    					<td><?php echo $stock->item_name; ?> (<?php echo $stock->net_weight; ?>)
<a type="button" class="ajax-modal" data-toggle="modal" href="#ajaxModal" data-title="Edit item" data-url="<?php echo site_url("items/edit/{$stock->item_id}/ajax") . "?no_delete=1&next=" . uri_string(); ?>" style="margin-right: 5px"><span class="glyphicon glyphicon-pencil"></span></a>
	    					</td>
	    					<td class="text-right"><?php echo $stock->quantity; ?></td>
	    					<td class="text-right"><?php echo number_format($stock->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($stock->price * $stock->quantity);
	    					echo number_format(($stock->price * $stock->quantity),2); 
	    					?></td>
	    					<?php if( hasAccess('inventory', 'purchases', 'edit') ) { ?>
	    					<td class="hidden-print">
	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("purchases/edit_item/{$stock->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button> <a href="<?php echo site_url("purchases/delete_item/{$stock->id}/{$purchase->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
						<tr>
	    					<td>TOTAL</td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"><strong><?php echo number_format($total,2); ?></strong></td>
	    					<td class="hidden-print"></td>
	    				</tr>
	    			</tbody>
	    		</table>


<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>