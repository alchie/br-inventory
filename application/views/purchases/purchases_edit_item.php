<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Search Item</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
	<div class="col-md-9">
				<div class="form-group">
				<label>Item</label>
	    			<select name="item_id" class="form-control">
	    			<?php foreach($items as $item) { ?>
	    				<option value="<?php echo $item->item_id; ?>" <?php echo ($item->item_id==$current_item->item_id) ? 'SELECTED' : ''; ?>><?php echo $item->item_id; ?> : <?php echo $item->item_name; ?> <?php echo ($item->net_weight)?" ({$item->net_weight})":" "; ?> - (P<?php echo number_format($item->last_price,2); ?>)</option>
	    			<?php } ?>
	    			</select>
	    		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label>Content</label>
			<input name="content" type="text" class="form-control text-center" value="<?php echo $current_item->content; ?>" id="product_content"> 
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
				<div class="form-group">
	    			<label>Quantity</label>
	    			<input name="quantity" type="text" class="form-control text-right" value="<?php echo $current_item->quantity; ?>">
	    		</div>
	</div>
	<div class="col-md-6">
				<div class="form-group">
	    			<label>Price per unit</label>
	    			<input name="price" type="text" class="form-control text-right" value="<?php echo number_format($current_item->price,2); ?>">
	    		</div>
	</div>
</div>

<?php if( isset($output) && ($output=='ajax') ) { ?>
	    		<div class="form-group">
	    			<a href="<?php echo site_url("purchases/delete_item/{$current_item->id}") . "?next=" . $this->input->get('next'); ?>" class="btn btn-danger btn-xs confirm">Delete this item</a>
	    		</div>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("purchases/items/{$purchase_id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>