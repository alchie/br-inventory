<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<a class="hidden-print pull-right" href="<?php echo site_url("purchases/items_order_print/{$order_id}"); ?>" target='_blank' style="margin-right: 10px;"><span class="glyphicon glyphicon-print"></span></a>

	    		<h3 class="panel-title">Purchased Items - <strong><?php echo date('m/d/Y', strtotime($order->date_order)); ?> &middot; <?php echo $order->requester; ?></strong></h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>

<?php if( $stocks ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th width="20px">ItemID</th>
	    					<th>ItemName</th>
	    					<th class="text-right">Quantity</th>
	    					<th class="text-right">PricePerUnit</th>
	    					<th class="text-right">TotalPrice</th>
	    					<th class="text-right">Date Purchase</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$total = 0;
	    			foreach($stocks as $stock) { ?>
	    				<tr>
	    					<td><?php echo $stock->item_id; ?></td>
	    					<td><?php echo $stock->item_name; ?> (<?php echo $stock->net_weight; ?>)
<a type="button" class="ajax-modal" data-toggle="modal" href="#ajaxModal" data-title="Edit item" data-url="<?php echo site_url("items/edit/{$stock->item_id}/ajax") . "?no_delete=1&next=" . uri_string(); ?>" style="margin-right: 5px"><span class="glyphicon glyphicon-pencil"></span></a>
	    					</td>
	    					<td class="text-right"><?php echo $stock->quantity; ?></td>
	    					<td class="text-right"><?php echo number_format($stock->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($stock->price * $stock->quantity);
	    					echo number_format(($stock->price * $stock->quantity),2); 
	    					?></td>
	    					<td class="text-right"><a href="<?php echo site_url("purchases/items/{$stock->purchase_id}"); ?>"><?php echo date('m/d/Y', strtotime($stock->date_purchase)); ?></a></td>
	    				</tr>
	    			<?php } ?>
						<tr>
	    					<td>TOTAL</td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"><strong><?php echo number_format($total,2); ?></strong></td>
	    					<td class="text-right"></td>
	    				</tr>
	    			</tbody>
	    		</table>


<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>