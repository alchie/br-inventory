<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Purchase</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
	    		<div class="form-group">
	    			<label>Date Start</label>
	    			<input name="date_start" type="text" class="form-control datepicker text-center" value="<?php echo ($this->input->post('date_start')) ? $this->input->post('date_start') : date('m/d/Y'); ?>">
	    		</div>

				<div class="form-group">
	    			<label>Date End</label>
	    			<input name="date_end" type="text" class="form-control datepicker text-center" value="<?php echo ($this->input->post('date_end')) ? $this->input->post('date_end') : date('m/d/Y'); ?>">
	    		</div>

				<div class="form-group">
				<label>Item</label>
	    			<select name="item_id" class="form-control autofocus product_select">
	    			<?php foreach($items as $item) { ?>
	    				<option value="<?php echo $item->item_id; ?>"><?php echo $item->item_name; ?><?php echo ($item->net_weight)?" ({$item->net_weight})":" "; ?></option>
	    			<?php } ?>
	    			</select>
	    		</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("system_names"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>