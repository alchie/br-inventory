<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<?php if( hasAccess('inventory', 'purchases', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Purchase" data-url="<?php echo site_url("purchases/add/ajax") . "?next=" . uri_string(); ?>">Add Purchase</button>


 <a style="margin-right: 10px;" class="pull-right ajax-modal" data-toggle="modal" href="#ajaxModal" data-target="#ajaxModal" data-title="Analysis" data-url="<?php echo site_url("purchases/analysis/ajax"); ?>"><span class="glyphicon glyphicon-signal"></span></a>

<?php } ?>
	    		<h3 class="panel-title">Purchases List</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $purchases ) { ?>
	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Purchase Date</th>
	    					<th>Purchaser</th>
	    					<th class="text-right">Cost</th>
	    					<?php if( hasAccess('inventory', 'purchases', 'edit') ) { ?>
	    					<th width="150px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($purchases as $purchase) { ?>
	    				<tr>
	    					<td><?php echo date('m/d/Y', strtotime($purchase->date_purchase)); ?></td>
	    					<td><?php echo $purchase->purchaser; ?></td>
	    					<td class="text-right"><?php echo number_format($purchase->cost,2); ?></td>
	    					<?php if( hasAccess('inventory', 'purchases', 'edit') ) { ?>
	    					<td>
	    					<a href="<?php echo site_url("purchases/items/{$purchase->id}"); ?>" class="btn btn-primary btn-xs">Items</a>
	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Purchase" data-url="<?php echo site_url("purchases/edit/{$purchase->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button>
<?php if( $purchase->order_id ) { ?>
	<a href="<?php echo site_url("orders/items/{$purchase->order_id}"); ?>" class="btn btn-info btn-xs">Order</a>
<?php } ?>
	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>