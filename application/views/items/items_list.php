<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('alpha_list'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('products', 'items', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal hidden-print" data-toggle="modal" data-target="#ajaxModal" data-title="Add Item" data-url="<?php echo site_url("items/add/ajax") . "?next=" . uri_string(); ?>">Add Item</button>
<?php } ?>

<a class="hidden-print pull-right" href="<?php echo site_url("items/print_all"); ?>" target='_blank' style="margin-right: 10px;"><span class="glyphicon glyphicon-print"></span></a>

<a class="hidden-print pull-right" href="<?php echo site_url("stocks/analytics"); ?>" target='_blank' style="margin-right: 10px;"><span class="glyphicon glyphicon-signal"></span></a>

	    		<h3 class="panel-title">Items List (<strong><?php echo $all_items; ?> Item<?php echo ($all_items>1) ? 's':''; ?></strong>)
<?php if( isset($page_id) && ($page_id=='archived') ) { ?>
<small><a class="hidden-print" href="<?php echo site_url("items"); ?>">Active</a></small>
<?php } else { ?>
<small><a class="hidden-print" href="<?php echo site_url("items/archived"); ?>">Archived</a></small>
<?php } ?>
	    		</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $items ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th>ID</th>
	    					<th>Item name</th>
	    					<th>Net Weight</th>
	    					<th>Content</th>
	    					<th>Minimum</th>
	    					<th>Category</th>
	    					<th>Store</th>
	    					<?php if( hasAccess('products', 'items', 'edit') ) { ?>
	    					<th width="150px" class="hidden-print">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($items as $item) { ?>
	    				<tr>
	    					<td><?php echo $item->item_id; ?></td>
	    					<td><?php echo $item->item_name; ?></td>
	    					<td><?php echo $item->net_weight; ?></td>
	    					<td><?php echo $item->content; ?></td>
	    					<td><?php echo $item->minimum; ?></td>
	    					<td>
								<a href="<?php echo site_url("items/category/{$item->category_id}"); ?>"><?php echo $item->category_name; ?></a>
	    					</td>
	    					<td>
								<a href="<?php echo site_url("items/store/{$item->store_id}"); ?>"><?php echo $item->store_name; ?></a>
	    					</td>
	    					<?php if( hasAccess('products', 'items', 'edit') ) { ?>
	    					<td class="hidden-print">

	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit item" data-url="<?php echo site_url("items/edit/{$item->item_id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button>
<?php if( $item->stocks_items ) { ?>
<a class="btn btn-success btn-xs" href="<?php echo site_url("stocks/entries/{$item->item_id}"); ?>">Entries</a>

<a class="hidden-print pull-right" href="<?php echo site_url("stocks/below_minimum/{$item->item_id}"); ?>" target='_blank'><span class="glyphicon glyphicon-signal"></span></a>
<?php } ?>
	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo (isset($pagination) && ($pagination!='')) ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>