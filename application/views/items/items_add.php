<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Item</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
	    		<div class="form-group">
	    			<label>Item Name</label>
	    			<input name="item_name" type="text" class="form-control" value="<?php echo $this->input->post('item_name'); ?>">
	    		</div>

<div class="row">
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Net Weight</label>
	    			<input name="net_weight" type="text" class="form-control" value="<?php echo $this->input->post('net_weight'); ?>">
	    		</div>
	</div>
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Content</label>
	    			<input name="content" type="text" class="form-control text-right" value="<?php echo ($this->input->post('content'))?$this->input->post('content'):1; ?>">
	    		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Category</label>
	    			<select class="form-control" name="category_id">
	    			<option value="">- - No Category - -</option>
	    			<?php foreach($categories as $category) { ?>
	    				<option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
	    			<?php } ?>
	    			</select>
	    		</div>
	</div>
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Store</label>
	    			<select class="form-control" name="store_id">
	    			<option value="">- - No Store - -</option>
	    			<?php foreach($stores as $store) { ?>
	    				<option value="<?php echo $store->id; ?>"><?php echo $store->name; ?></option>
	    			<?php } ?>
	    			</select>
	    		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Minimum Stocks</label>
	    			<input name="minimum" type="text" class="form-control text-center" value="<?php echo ($this->input->post('minimum')) ? $this->input->post('minimum') : 1; ?>">
	    		</div>
	</div>
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Shelf</label>
	    			<input name="shelf" type="text" class="form-control text-center" value="<?php echo ($this->input->post('minimum')) ? $this->input->post('minimum') : ''; ?>">
	    		</div>
	</div>
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("items"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>