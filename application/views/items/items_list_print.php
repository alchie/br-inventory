<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Print</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">

	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">ID <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'item_id','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th>Item name <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'item_name','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th class="text-center">Net Weight</th>
	    					<th class="text-center">Category <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'category','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th class="text-center">Content</th>
	    					<th class="text-center">Minimum</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($items as $item) { ?>
	    				<tr>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
	    					<td><?php echo $item->item_name; ?> <a href="<?php echo site_url("items/edit/{$item->item_id}") . "?next=" . uri_string(); ?>" class="hidden_print" target="_edit_item"><span class="fa fa-pencil"></span></a></td>
	    					<td class="text-center"><?php echo $item->net_weight; ?></td>
	    					<td class="text-center"><?php echo $item->category_name; ?></td>
	    					<td class="text-right"><?php echo $item->content; ?></td>
	    					<td class="text-right"><?php echo $item->minimum; ?></td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

  </body>
</html>
