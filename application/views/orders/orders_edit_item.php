<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Search Item</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>



<div class="row">
	<div class="col-md-6">
		<div class="form-group">
	    			<label>Date</label>
	    			<input name="item_date" type="text" class="form-control text-center datepicker" value="<?php echo date('m/d/Y', strtotime($current_item->item_date)); ?>">
	    </div>
	</div>

	<div class="col-md-6">
				<div class="form-group">
	    			<label>Receipt Number</label>
	    			<input name="receipt_id" type="text" class="form-control text-right" value="<?php echo $current_item->receipt_id; ?>">
	    		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
				<div class="form-group">
				<label>Item</label>
	    			<select name="item_id" class="form-control">
	    			<?php foreach($items as $item) { ?>
	    				<option value="<?php echo $item->item_id; ?>" <?php echo ($item->item_id==$current_item->item_id) ? 'SELECTED' : ''; ?>><?php echo $item->item_id; ?> : <?php echo $item->item_name; ?> - <?php echo $item->net_weight; ?> - &#x20B1;<?php echo number_format($item->last_price,2); if($item->content > 1) { ?> - [<?php echo $item->content; ?> / &#x20B1;<?php echo number_format(($item->last_price/$item->content),2); ?>]<?php } ?></option>
	    			<?php } ?>
	    			</select>
	    		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
			<div class="form-group">
	    			<label>Quantity</label>
	    			<input name="quantity" type="text" class="form-control text-right" value="<?php echo $current_item->quantity; ?>">
	    		</div>
	</div>
	<div class="col-md-6">
				<div class="form-group">
	    			<label>Price per item</label>
	    			<input name="price" type="text" class="form-control text-right" value="<?php echo number_format($current_item->price,2); ?>">
	    		</div>
	</div>
</div>
<?php if( isset($output) && ($output=='ajax') ) { ?>
	    		<div class="form-group">
	    			<a href="<?php echo site_url("orders/delete_item/{$current_item->id}") . "?next=" . $this->input->get('next'); ?>" class="btn btn-danger btn-xs confirm">Delete this item</a>
	    		</div>
<?php } ?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("purchases/items/{$purchase_id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>