<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stocks Consumption Report</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">
<div class="header-label">
<div class="pull-right">
<?php if($by_cat) { ?>
		<a href="<?php echo site_url("orders/report_dates/{$start_date}/{$end_date}/0"); ?>" class="hidden_print">By Dates</a>
<?php } else { ?>
	<a href="<?php echo site_url("orders/report_dates/{$start_date}/{$end_date}/1"); ?>" class="hidden_print">By Category</a>
<?php } ?>
</div>
<h1>Bishop's Residence</h1>
<h2>Stocks Consumption Report <?php if($by_cat) { ?>By Category<?php } ?></h2>
<?php if($by_cat) { ?>
<h3><?php echo date("F d, Y", strtotime($dates[0]->item_date)); ?></h3>
<?php } else { ?>
<h3><?php echo date("F d, Y", strtotime($start_date)); ?> - <?php echo date("F d, Y", strtotime($end_date)); ?></h3>
<?php } ?>
<center class="pagination hidden_print">
<?php echo $pagination; ?>
</center>
</div>
<?php 
$total = array();
$grand_total = 0;
foreach($categories as $category) {
	$total[$category->category_id] = 0; 
}
?>
	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">#</th>
	    					<th class="text-center">ID <a href="<?php echo site_url(uri_string()) . "?sort=item_id&order_by=" . (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'); ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th>Item name <a href="<?php echo site_url(uri_string()) . "?sort=item_name&order_by=" . (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'); ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<?php foreach($categories as $category) { ?>
	    						<th class="text-center">
	    						<?php if($this->input->get('filter_category')) { ?>
	    						<?php echo $category->category_name; ?> <a class="hidden_print" href="<?php echo site_url(uri_string()); ?>">x</a>
	    						<?php } else { ?>
	    						<a class="hidden_print" href="<?php echo site_url(uri_string()) . "?filter_category=" . $category->category_id; ?>"><?php echo $category->category_name; ?> </a>
	    						<?php } ?>
	    						</th>
	    					<?php } ?>
	    					<?php if(!$this->input->get('filter_category')) { ?>
	    					<th class="text-right">Total</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$prev_item_id = 0;
$n=1;
	    			$stock_total = array(); 
$stocks_arr = array();

foreach($stocks as $stock) {  
	if( !isset($stocks_arr[$stock->item_id]) ) {
		$stocks_arr[$stock->item_id] = array();
	}
	if( !isset($stocks_arr[$stock->item_id][ $stock->category_id ]) ) {
		$stocks_arr[$stock->item_id][ $stock->category_id ] = 0;
	}
	$stocks_arr[$stock->item_id][ $stock->category_id ] += $stock->total_amount;
}

	    			foreach($items as $item) { 
	    				if( !isset($stock_total[$item->item_id]) ) {
	    						$stock_total[$item->item_id] = 0;
	    					}
	    				?>
	    				<tr class="<?php echo ($prev_item_id==$item->item_id) ? "highlight_td" : ""; ?>">
	    					<td class="text-center"><?php echo $n++; ?></td>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
	    					<td><?php echo $item->item_name; ?></td>
	    					<?php foreach($categories as $category) {
	    						$total_amount = (isset($stocks_arr[$item->item_id][$category->category_id])) ? $stocks_arr[$item->item_id][$category->category_id] : 0;
	    						$stock_total[$item->item_id] += $total_amount;
	    						?>
	    						<td class="text-right"><?php echo (isset($stocks_arr[$item->item_id][$category->category_id])) ? number_format($total_amount,2) : ''; $total[$category->category_id] += $total_amount;  ?></td>
	    					<?php } ?>
	    					<?php if(!$this->input->get('filter_category')) { ?>
	    					<td class="text-right"><?php echo number_format($stock_total[$item->item_id],2); ?></td>
	    					<?php } ?>
	    				</tr>
	    			<?php 
	    			$prev_item_id = $item->item_id;
	    			 } ?>
						<tr>
	    					<td  colspan="3" class="text-right bold highlight_td">TOTAL</td>
	    					<?php foreach($categories as $category) { 
	    						$grand_total += $total[$category->category_id];
	    						?>
	    						<td class="text-right bold highlight_td"><?php echo number_format($total[$category->category_id],2); ?></td>
	    					<?php } ?>
	    					<?php if(!$this->input->get('filter_category')) { ?>
	    					<td class="text-right bold highlight_td"><?php echo number_format($grand_total,2); ?></td>
	    					<?php } ?>
	    				</tr>
	    			</tbody>
	    		</table>

  </body>
</html>
