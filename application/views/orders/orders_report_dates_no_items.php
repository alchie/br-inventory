<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stocks Consumption Report</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">
<div class="header-label">
<div class="pull-right">
	<?php if($by_cat) { ?>
<a href="<?php echo site_url("orders/report_dates/{$start_date}/{$end_date}/0"); ?>" class="hidden_print">By Dates</a>
<?php } else { ?>
<a href="<?php echo site_url("orders/report_dates/{$start_date}/{$end_date}/1"); ?>" class="hidden_print">By Category</a>
<?php } ?>
</div>
<h1>Bishop's Residence</h1>
<h2>Stocks Consumption Report <?php if($by_cat) { ?>By Category<?php } ?></h2>
<h3><?php echo date("F d, Y", strtotime($start_date)); ?><?php if(!$by_cat) { ?> - <?php echo date("F d, Y", strtotime($end_date)); ?><?php } ?></h3>
</div>

<h3 class="text-center">No Items Found!</h3>

  </body>
</html>
