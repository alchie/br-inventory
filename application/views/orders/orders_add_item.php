<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('products', 'items', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal hidden-print" data-toggle="modal" data-target="#ajaxModal" data-title="Add Item" data-url="<?php echo site_url("items/add/ajax") . "?next=" . uri_string(); ?>">Add Item</button>
<?php } ?>
	    		<h3 class="panel-title">Pull-out Item : <?php echo date('F d, Y', strtotime($order->date_order)); ?> <?php echo $order->requester; ?></h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if( isset($new_item) && ($new_item) ) { ?>
<div class="alert alert-success">
	<strong>Added Successfully!</strong> <a class="ajax-modal badge" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("items/edit/{$new_item->item_id}/ajax") . "?no_delete=1&next=" . uri_string(); ?>"><?php echo $new_item->item_id; ?> : <?php echo $new_item->item_name; ?> (<?php echo $new_item->net_weight; ?>)</a> <a class="ajax-modal badge" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("orders/edit_item/{$new_item->id}/ajax") . "?no_delete=1&next=" . uri_string(); ?>"><?php echo $new_item->quantity; ?> item(s)  x  <?php echo number_format($new_item->price,2); ?> = <?php echo number_format(($new_item->quantity * $new_item->price),2); ?></a>
</div>
<?php } ?>

<?php if( isset($new_product) && ($new_product) ) { ?>
<div class="alert alert-<?php echo ($this->input->get('update_product')) ? 'warning' : 'success'; ?>">
	<strong><?php echo ($this->input->get('update_product')) ? 'Product Updated' : 'New Product Added'; ?>!</strong> <a class="ajax-modal badge" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("items/edit/{$new_product->item_id}/ajax") . "?no_delete=1&next=" . uri_string(); ?>"><?php echo $new_product->item_id; ?> : <?php echo $new_product->item_name; ?> (<?php echo $new_product->net_weight; ?>)</a></span>
</div>
<?php } ?>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
	    			<label>Date</label> <a href="javascript:void(0);" id="increaseItemDate" class="pull-right"><span class="glyphicon glyphicon-plus-sign"></span></a> <a href="javascript:void(0);" id="decreaseItemDate"><span class="glyphicon glyphicon-minus-sign"></span></a>
	    			<input id="input_item_date" name="item_date" type="text" class="form-control text-center datepicker" value="<?php echo date('m/d/Y', strtotime($last_id->item_date)); ?>">
	    </div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
	    	<label>Receipt #</label> <a href="javascript:void(0);" id="increaseRecieptId" class="pull-right"><span class="glyphicon glyphicon-plus-sign"></span></a> <a href="javascript:void(0);" id="decreaseRecieptId"><span class="glyphicon glyphicon-minus-sign"></span></a>
	    		<input id="input_receipt_id" name="receipt_id" type="text" class="form-control text-center" value="<?php echo $last_id->receipt_id; ?>">
	    		
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-md-9">
				<div class="form-group">
				<label>Item</label>
	    			<select name="item_id" class="form-control autofocus product_select">
	    			<?php foreach($items as $item) { ?>
	    				<option <?php echo (isset($new_product) && ($new_product) && ($new_product->item_id==$item->item_id)) ? " SELECTED" : ""; ?> value="<?php echo $item->item_id; ?>"><?php echo $item->item_id; ?> : <?php echo $item->item_name; ?> - <?php echo $item->net_weight; ?> - &#x20B1;<?php echo number_format($item->last_price,2); if($item->content > 1) { ?> - [<?php echo $item->content; ?> / &#x20B1;<?php echo number_format(($item->last_price/$item->content),2); ?>]<?php } ?></option>
	    			<?php } ?>
	    			</select>
	    		</div>
	</div>
	<div class="col-md-3">
				<div class="form-group">
	    			<label>Quantity</label> <a href="javascript:void(0);" id="increaseQuantity" class="pull-right"><span class="glyphicon glyphicon-plus-sign"></span></a> <a href="javascript:void(0);" id="decreaseQuantity"><span class="glyphicon glyphicon-minus-sign"></span></a>
	    			<input name="quantity" type="text" class="form-control text-center" value="1" id="product_quantity">
	    		</div>
	</div>
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    	<span class="well pull-right"><sup>PHP</sup> <strong><?php echo number_format($order->total,2); ?></strong></span>
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("orders/items/{$order_id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>

<?php if( $last_added ) { ?>
<div class="row">
    	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">

<div class="list-group">
<?php foreach($last_added as $lasta) { ?>
  <a class="ajax-modal list-group-item" data-toggle="modal" href="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("orders/edit_item/{$lasta->id}/ajax") . "?next=" . uri_string(); ?>"><?php echo $lasta->receipt_id; ?> : <strong><?php echo $lasta->item_name; ?> <?php echo ($item->net_weight)?"({$lasta->net_weight})":" "; ?></strong> <span class="pull-right"><?php echo $lasta->quantity; ?> item(s)  x  <?php echo number_format($lasta->price,2); ?> = <strong><?php echo number_format(($lasta->quantity * $lasta->price),2); ?></strong></span></a>
<?php } ?>
</div>

	    </div>
	    </div>
</div>
<?php } ?>

</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>