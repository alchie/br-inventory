<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">


<?php if( hasAccess('inventory', 'orders', 'add') ) { ?>
 <a class="btn btn-info btn-xs pull-right hidden-print" href="<?php echo site_url("orders/add_item/{$order_id}"); ?>">Pull-out Item</a>
<?php } ?>

<?php if( hasAccess('inventory', 'purchases', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Purchase" data-url="<?php echo site_url("purchases/add/ajax") . "?order_id={$order_id}"; ?>" style="margin-right: 5px">Add Purchase</button>
<?php } ?>

<?php if( $order->purchases ) { ?>
 <a class="btn btn-primary btn-xs pull-right hidden-print" style="margin-right: 5px" href="<?php echo site_url("purchases/order/{$order_id}"); ?>">Purchases</a>
<?php } ?>


<a class="hidden-print pull-right" href="<?php echo site_url("orders/items_print/{$order_id}"); ?>" target='_blank' style="margin-right: 10px;" title="Print"><span class="glyphicon glyphicon-print"></span></a>

<?php if($this->input->get('remaining')==1) { ?>
<a class="hidden-print pull-right" href="<?php echo site_url("orders/items/{$order_id}"); ?>" style="margin-right: 10px;" title="Remaining"><span class="glyphicon glyphicon-remove"></span></a>
<?php } else { ?>
<a class="hidden-print pull-right" href="<?php echo site_url("orders/items/{$order_id}"); ?>?remaining=1" style="margin-right: 10px;" title="Remaining"><span class="glyphicon glyphicon-shopping-cart"></span></a>
<?php } ?>
	    		<h3 class="panel-title">Ordered Items - <strong><?php echo date('m/d/Y', strtotime($order->date_order)); ?> &middot; <?php echo $order->requester; ?></strong>
<a class="ajax-modal hidden-print" data-toggle="modal" href="#ajaxModal" data-title="Edit Order" data-url="<?php echo site_url("orders/edit/{$order->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
	    		</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $stocks ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    				<th width="100px">Date</th>
	    				<th width="20px">Rcpt#</th>
	    					<th width="20px">ItemID</th>
	    					<th width="800px">ItemName</th>
	    					<th class="text-right">Quantity</th>
	    					<th class="text-right">PricePerUnit</th>
	    					<th class="text-right">TotalPrice</th>
	    					<?php if( hasAccess('inventory', 'orders', 'edit') ) { ?>
	    					<th width="120px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$total = 0;

	    			foreach($stocks as $stock) { 
	    				if(($this->input->get('remaining')==1) && (($stock->quantity-$stock->purchased_items)<=0)) { continue; } 
	    				?>
	    				<tr>
	    					<td><?php echo date('m/d/Y', strtotime($stock->item_date)); ?></td>
	    					<td><?php echo $stock->receipt_id; ?></td>
	    					<td><?php echo $stock->item_id; ?></td>
	    					<td><?php echo $stock->item_name; ?> (<?php echo $stock->net_weight; ?>)</td>
	    				<?php if($this->input->get('remaining')==1) { ?>
	    					<td class="text-right"><?php echo ($stock->quantity-$stock->purchased_items); ?></td>
	    					<td class="text-right"><?php echo number_format($stock->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($stock->price * ($stock->quantity-$stock->purchased_items));
	    					echo number_format(($stock->price * ($stock->quantity-$stock->purchased_items)),2); 
	    					?></td>
	    				<?php } else { ?>
	    					<td class="text-right"><?php echo $stock->quantity; ?></td>
	    					<td class="text-right"><?php echo number_format($stock->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($stock->price * $stock->quantity);
	    					echo number_format(($stock->price * $stock->quantity),2); 
	    					?></td>
	    				<?php } ?>
	    					<?php if( hasAccess('inventory', 'orders', 'edit') ) { ?>
	    					<td>
	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("orders/edit_item/{$stock->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button> <a href="<?php echo site_url("orders/delete_item/{$stock->id}/{$order->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
						<tr>
	    					<td>TOTAL</td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"></td>
	    					<td class="text-right"><strong><?php echo number_format($total,2); ?></strong></td>
	    					<td></td>
	    				</tr>
	    			</tbody>
	    		</table>


<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>