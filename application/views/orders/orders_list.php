<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('inventory', 'orders', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Order" data-url="<?php echo site_url("orders/add/ajax") . "?next=" . uri_string(); ?>">Add Order</button>
<?php } ?>
	    		<h3 class="panel-title">Orders List</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $orders ) { ?>
	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Order Date</th>
	    					<th>Requester</th>
	    					<th class="text-right">Budget</th>
	    					<?php if( hasAccess('inventory', 'orders', 'edit') ) { ?>
	    					<th width="200px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($orders as $order) { ?>
	    				<tr>
	    					<td><?php echo date('m/d/Y', strtotime($order->date_order)); ?></td>
	    					<td><?php echo $order->requester; ?></td>
	    					<td class="text-right"><?php echo number_format($order->budget,2); ?></td>
	    					<?php if( hasAccess('inventory', 'orders', 'edit') ) { ?>
	    					<td>
	    					<a href="<?php echo site_url("orders/items/{$order->id}"); ?>" class="btn btn-primary btn-xs">Items</a>
	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Order" data-url="<?php echo site_url("orders/edit/{$order->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button>
	    					<?php if( $order->purchases ) { ?>
	    					<a href="<?php echo site_url("purchases/order/{$order->id}"); ?>" class="btn btn-info btn-xs">Purchases</a>
	    					<?php } ?>
	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>