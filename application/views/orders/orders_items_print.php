<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Print</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">
<div class="header-label">
<div style="float: right">
<a href="<?php echo site_url(uri_string()) . "?remaining=1"; ?>" class="hidden_print">Remaining</a> &middot;  
<a href="<?php echo site_url(uri_string()) . "?check=1"; ?>" class="hidden_print">Check Differences</a> &middot; 
<a href="<?php echo site_url(uri_string()); ?>" class="hidden_print">Default (by Category ASC)</a> 
</div>
<h1>Bishop's Residence <a href="<?php echo site_url("orders/analytics/{$order->id}"); ?>" class="hidden_print">Analytics</a></h1> 
<h2>Purchase Request</h2>
<h3><?php echo date("F d, Y", strtotime($order->date_order)); ?></h3>
<?php if($trolley) { ?>
<center class="hidden_print" style="font-size: 14px;font-weight: bold">
<?php if($this->input->get('trolley')) { ?>
<a href="<?php echo site_url(uri_string()) . append_querystring("trolley", 0); ?>">Show All</a>
<?php } else { ?>
<?php foreach($trolley as $troll) { ?>
	<a href="<?php echo site_url(uri_string()) . append_querystring("trolley", $troll->trolley); ?>"><?php echo $troll->trolley; ?></a>
<?php } ?>
<?php } ?>
</center>
<?php } ?>
</div>
<?php 
$grand_total = 0;
?>
<h3>Stock Replinishment</h3>
	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">#</th>
	    					<th class="text-center">ID <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'item_id','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
							<th class="text-center">Quantity <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'quantity','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th>Item name <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'item_name','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th class="text-center">Category <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'category','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th class="text-center">Store <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'store','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					
	    					<th class="text-center">Price per Unit <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'price','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th class="text-center">Total <a href="<?php echo site_url(uri_string()) . append_querystring(array('sort' => 'total','order_by' => (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'))) ; ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$prev_item_id = 0;
$n=1;
	    			$total = 0; 
	    			foreach($stocks as $item) { 
						if( (!$item->receipt_id) || ((int) $item->price==0) ) { continue; }
						if(($this->input->get('remaining')==1) && ($item->remaining_quantity<=0)) { continue; }
						

	    				?>
	    				<tr class="<?php echo ($prev_item_id==$item->item_id) ? "highlight_td" : ""; ?>">
	    					<td class="text-center"><?php echo $n++; ?></td>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
<?php if($this->input->get('remaining')==1) { ?>
	    					<td class="text-center"><?php echo ceil($item->remaining_quantity/$item->content); ?></td>
<?php } else { ?>
	    					<td class="text-center"><?php echo ceil($item->total_quantity/$item->content); ?></td>
<?php } ?>
	    					<td><?php echo $item->item_name; ?> <?php echo ($item->net_weight) ? "({$item->net_weight})" : ''; ?></td>
	    					<td class="text-center"><?php echo $item->category_name; ?></td>
	    					<td class="text-center"><?php echo $item->store_name; ?></td>
<?php if($this->input->get('remaining')==1) { ?>
	    					
	    					<td class="text-center"><?php echo number_format($item->last_price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += (ceil($item->remaining_quantity/$item->content) * $item->last_price); 
	    					echo number_format((ceil($item->remaining_quantity/$item->content) * $item->last_price),2); 
	    					$grand_total += (ceil($item->remaining_quantity/$item->content) * $item->last_price) ;
	    					?></td>
<?php } else { ?>
	    					<td class="text-center"><?php echo number_format($item->last_price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += (ceil($item->total_quantity/$item->content) * $item->last_price); 
	    					echo number_format((ceil($item->total_quantity/$item->content) * $item->last_price),2); 
	    					$grand_total += (ceil($item->total_quantity/$item->content) * $item->last_price) ;
	    					?></td>
<?php } ?>
	    				</tr>
	    			<?php 
	    			$prev_item_id = $item->item_id;
	    			 } ?>
						<tr>
	    					<td  colspan="7" class="text-right bold highlight_td">TOTAL</td>
	    					<td class="text-right bold highlight_td"><?php echo number_format($total,2); ?></td>
	    				</tr>
	    			</tbody>
	    		</table>
<br>
<h3>New Items / Increase in stocks</h3>
	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">#</th>
	    					<th class="text-center">ID</th>
	    					<th class="text-center">Quantity</th>
	    					<th>Item name</th>
	    					<th class="text-center">Category</th>
	    					<th class="text-center">Store</th>
	    					
	    					<th class="text-center">Estimated Price</th>
	    					<th class="text-center">Total</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php $total = 0; 
	    			foreach($stocks as $item) { 
						if( (!$item->receipt_id) || ((int) $item->price==0) ) {
						if(($this->input->get('remaining')==1) && ($item->remaining_quantity<=0)) { continue; } 
	    				?>
	    				<tr>
	    					<td class="text-center"><?php echo $n++; ?></td>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
<?php if($this->input->get('remaining')==1) { ?>
	    					<td class="text-center"><?php echo $item->remaining_quantity; ?></td>
<?php } else { ?>
	    					<td class="text-center"><?php echo $item->total_quantity; ?></td>
<?php } ?>
	    					<td><?php echo $item->item_name; ?> <?php echo ($item->net_weight) ? "({$item->net_weight})" : ''; ?></td>
	    					<td class="text-center"><?php echo $item->category_name; ?></td>
	    					<td class="text-center"><?php echo $item->store_name; ?></td>
<?php if($this->input->get('remaining')==1) { ?>
	    					
	    					<td class="text-center"><?php echo number_format($item->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($item->remaining_quantity * $item->price); 
	    					echo number_format(($item->remaining_quantity * $item->price),2); 
	    					$grand_total += ($item->remaining_quantity * $item->price) ;
	    					?></td>
<?php } else { ?>
	    					
	    					<td class="text-center"><?php echo number_format($item->price,2); ?></td>
	    					<td class="text-right"><?php 
	    					$total += ($item->total_quantity * $item->price); 
	    					echo number_format(($item->total_quantity * $item->price),2); 
	    					$grand_total += ($item->total_quantity * $item->price) ;
	    					?></td>
<?php } ?>
	    				</tr>
	    			<?php 
	    		}
	    			 } ?>
						<tr>
	    					<td  colspan="7" class="text-right bold highlight_td">TOTAL</td>
	    					<td class="text-right bold highlight_td"><?php echo number_format($total,2); ?></td>
	    				</tr>
	    			</tbody>
	    		</table>
<br>
<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
	    					<td  class="text-right bold highlight_td">GRAND TOTAL</td>
	    					<td class="text-right bold highlight_td"><?php echo number_format($grand_total,2); ?></td>
	    				</tr>
	    			</tbody>
	    		</table>

<h3 style="margin-top:40px">Requested by: <?php echo $order->requester; ?></h3>
  </body>
</html>
