<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Analytics</title>
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
    
  </head>
  <body class="items">
<div class="header-label">
<h1>Bishop's Residence <a href="<?php echo site_url("orders/items_print/{$order->id}"); ?>" class="hidden_print">Default</a></h1>
<h2>Items Pull-Out Analysis</h2>
<h3><?php echo date("F d, Y", strtotime($order->date_order)); ?></h3>
<center class="pagination hidden_print">
<?php echo $pagination; ?>
</center>
</div>
<?php 
$total = array();
$grand_total = 0;
foreach($dates as $date) {
	$total[$date->item_date] = 0; 
}
?>
<h3>Stock Replinishment</h3>
	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<thead>
	    				<tr>
	    					<th class="text-center">#</th>
	    					<th class="text-center">ID <a href="<?php echo site_url(uri_string()) . "?sort=item_id&order_by=" . (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'); ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<th>Item name <a href="<?php echo site_url(uri_string()) . "?sort=item_name&order_by=" . (($this->input->get('order_by') && ($this->input->get('order_by'))=='ASC') ? 'DESC' : 'ASC'); ?>" class="hidden_print"><span class="fa fa-sort"></span></a></th>
	    					<?php foreach($dates as $date) { 
	    						if( !$date->item_date ) { continue; }
	    						?>
	    						<th class="text-right"><?php echo date("m/d/Y", strtotime( $date->item_date ) ); ?></th>
	    					<?php } ?>
	    					<!--<th class="text-right">Total</th>-->
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php 
$prev_item_id = 0;
$n=1;
	    			$stock_total = array(); 
$stocks_arr = array();

foreach($stocks as $stock) { 
	if( !isset($stocks_arr[$stock->item_id]) ) {
		$stocks_arr[$stock->item_id] = array();
	}
	if( !isset($stocks_arr[$stock->item_id][ date("Y-m-d", strtotime($stock->item_date)) ]) ) {
		$stocks_arr[$stock->item_id][ date("Y-m-d", strtotime($stock->item_date)) ] = 0;
	}
	$stocks_arr[$stock->item_id][ date("Y-m-d", strtotime($stock->item_date)) ] += $stock->total_amount;
}

	    			foreach($items as $item) { 
	    				if( !isset($stock_total[$item->item_id]) ) {
	    						$stock_total[$item->item_id] = 0;
	    					}
	    				?>
	    				<tr class="<?php echo ($prev_item_id==$item->item_id) ? "highlight_td" : ""; ?>">
	    					<td class="text-center"><?php echo $n++; ?></td>
	    					<td class="text-center"><?php echo $item->item_id; ?></td>
	    					<td><?php echo $item->item_name; ?></td>
	    					<?php foreach($dates as $date) { 
	    						if( !$date->item_date ) { continue; }
	    						$total_amount = (isset($stocks_arr[$item->item_id][ date("Y-m-d", strtotime($date->item_date))])) ? $stocks_arr[$item->item_id][ date("Y-m-d", strtotime($date->item_date))] : 0;
	    						$stock_total[$item->item_id] += $total_amount;
	    						?>
	    						<td class="text-right"><?php echo (isset($stocks_arr[$item->item_id][ date("Y-m-d", strtotime($date->item_date))])) ? number_format($total_amount,2) : ''; $total[$date->item_date] += $total_amount;  ?></td>
	    					<?php } ?>
	    					<!--<td class="text-right"><?php echo number_format($stock_total[$item->item_id],2); ?></td>-->
	    				</tr>
	    			<?php 
	    			$prev_item_id = $item->item_id;
	    			 } ?>
						<tr>
	    					<td  colspan="3" class="text-right bold highlight_td">TOTAL</td>
	    					<?php foreach($dates as $date) { 
	    						if( !$date->item_date ) { continue; }
	    						$grand_total += $total[$date->item_date];
	    						?>
	    						<td class="text-right bold highlight_td"><?php echo number_format($total[$date->item_date],2); ?></td>
	    					<?php } ?>
	    					<!--<td class="text-right bold highlight_td"><?php echo number_format($grand_total,2); ?></td>-->
	    				</tr>
	    			</tbody>
	    		</table>


<h3 style="margin-top:40px">Requested by: <?php echo $order->requester; ?></h3>
  </body>
</html>
