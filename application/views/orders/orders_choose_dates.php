<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Order</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
<div class="row">
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>Start Date</label>
	    			<input name="date_start" type="text" class="form-control datepicker text-center" value="<?php echo ($this->input->post('date_start')) ? $this->input->post('date_start') : date('m/d/Y'); ?>">
	    		</div>
	</div>
	<div class="col-md-6">
	    		<div class="form-group">
	    			<label>End Date</label>
	    			<input name="date_end" type="text" class="form-control datepicker text-center" value="<?php echo ($this->input->post('date_end')) ? $this->input->post('date_end') : date('m/d/Y'); ?>">
	    		</div>
	</div>
</div>
	    		<div class="form-group">
	    			<label><input name="by_category" type="checkbox" value="1"> By Category</label>
	    			
	    		</div>
<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("orders"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>