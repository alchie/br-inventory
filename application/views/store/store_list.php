<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('products', 'store', 'add') ) { ?>
 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Store" data-url="<?php echo site_url("store/add/ajax") . "?next=" . uri_string(); ?>">Add Store</button>
<?php } ?>
	    		<h3 class="panel-title">Store List</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $stores ) { ?>
	    		<table class="table table-default table-hover hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Store Name</th>
	    					<?php if( hasAccess('products', 'store', 'edit') ) { ?>
	    					<th width="50px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($stores as $store) { ?>
	    				<tr>
	    					<td>
	    					<?php echo $store->name; ?>
	    					</td>
	    					<?php if( hasAccess('products', 'store', 'edit') ) { ?>
	    					<td>

	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Store" data-url="<?php echo site_url("store/edit/{$store->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button>


	    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>