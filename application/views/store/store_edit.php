<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<a href="<?php echo site_url("store/delete/{$item->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
	    		<h3 class="panel-title">Edit Store</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

	    		<div class="form-group">
	    			<label>Store Name</label>
	    			<input name="name" type="text" class="form-control" value="<?php echo $item->name; ?>">
	    		</div>
<?php if( isset($output) && ($output=='ajax') ) { ?>
	    		<div class="form-group">
	    			<a href="<?php echo site_url("store/delete/{$item->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this Store</a>
	    		</div>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("store"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>