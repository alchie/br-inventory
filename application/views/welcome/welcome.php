<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( count( $this->session->menu_module ) > 0 ) { ?>


    <div class="container" id="homepage">


<?php $this->load->view('alpha_list'); ?>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
          <a href="<?php echo site_url("orders"); ?>" class="btn btn-success btn-xs pull-right">More</a>
            <h3 class="panel-title">Latests Orders</h3>
          </div>
          <div class="panel-body list-group">
            <?php foreach($orders as $order) { ?>
                <a href="<?php echo site_url("orders/items/{$order->id}"); ?>" class="list-group-item">
                <span class="badge"><?php echo number_format($order->budget,2); ?></span>
                <?php echo date('m/d/Y', strtotime($order->date_order)); ?> - <?php echo $order->requester; ?></a>
            <?php } ?>
          </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
          <a href="<?php echo site_url("purchases"); ?>" class="btn btn-success btn-xs pull-right">More</a>
            <h3 class="panel-title">Latests Purchases</h3>
          </div>
          <div class="panel-body list-group">
            <?php foreach($purchases as $purchase) { ?>
                <a href="<?php echo site_url("purchases/items/{$purchase->id}"); ?>" class="list-group-item">
                <span class="badge"><?php echo number_format($purchase->cost,2); ?></span>
                <?php echo date('m/d/Y', strtotime($purchase->date_purchase)); ?> - <?php echo $purchase->purchaser; ?></a>
            <?php } ?>
          </div>
        </div>
    </div>
</div>

    </div>

<?php } else { ?>

<div class="container">
<div class="row">
    		<div class="col-md-4 col-md-offset-4">
    			<div class="panel panel-danger">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Account Restricted</h3>
    				</div>
    				<div class="panel-body text-center">
    				Your account have not been granted any access to the system! <br/> Please contact system administrator!
					</div>
    			</div>
    		</div>
</div>
</div>
<?php } ?>

<?php $this->load->view('footer'); ?>